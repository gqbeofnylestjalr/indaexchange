'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  console.log('EmberApp.env() = ' + EmberApp.env()); // TODO: separate env
  let app = new EmberApp(defaults, {
    // Add options here

    sassOptions: {
      extension: 'sass'
    }

    // ,fingerprint: {
    //   prepend: '/exchange/'
    // }

    // ,outputPaths: {
    //   vendor: {
    //     css: '/assets/ember.css',
    //     js: '/assets/ember.js'
    //   }
    // }

    // http://localhost:4200/exchange/assets/vendor-ddff20564c52a7b665d7ffeb7419caf9.js
    // http://localhost:4200/assets/vendor-ddff20564c52a7b665d7ffeb7419caf9.js

    // ,outputPaths: {
    //   app: {
    //     html: 'index.html',
    //     css: {
    //       'app': '/exchange/assets/indaexchange.css'
    //     },
    //     js: '/exchange/assets/indaexchange.js'
    //   },
    //   vendor: {
    //     css: '/exchange/assets/vendor.css',
    //     js: '/exchange/assets/vendor.js'
    //   }
    // }


  });

  // TODO: use Bower
  
  app.import('vendor/lib/cookies/cookies.js', {
    using: [
      { transformation: 'amd', as: 'cookies' }
    ]
  });

  app.import('vendor/lib/plotly/plotly-latest.min.js', {
    using: [
      { transformation: 'amd', as: 'plotly' }
    ]
  });

  app.import('vendor/lib/chart/Chart.min.js', {
    using: [
      { transformation: 'amd', as: 'Chart' }
    ]
  });

  app.import('vendor/lib/bootstrap/bootstrap.min.css'); // Version 4.1.3


/**
 * Represents a book.
 * @constructor
 * @param {string} title - The title of the book.
 * @param {string} author - The author of the book.
 */

  // app.import('vendor/lib/intlTelInput/intlTelInput.css');

  app.import('vendor/lib/bootstrap/bootstrap-select.min.css');



  



  // Add to top file vendor.css
 
  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  return app.toTree();
};
