import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | funds/balance', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:funds/balance');
    assert.ok(route);
  });
});
