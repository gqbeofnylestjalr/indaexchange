import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | funds/deposit', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:funds/deposit');
    assert.ok(route);
  });
});
