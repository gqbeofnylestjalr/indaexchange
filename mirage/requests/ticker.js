export default function() {  
  return `
    [
      {
        "base": "ETH",
        "quote": "BTC",
        "low": null,
        "high": null,
        "bid": null,
        "ask": null,
        "firstid": 248,
        "secondid": 100
      },
      {
        "base": "TRX",
        "quote": "BTC",
        "low": null,
        "high": null,
        "bid": null,
        "ask": null,
        "firstid": 404,
        "secondid": 100
      },
      {
        "base": "EOS",
        "quote": "BTC",
        "low": null,
        "high": null,
        "bid": null,
        "ask": null,
        "firstid": 416,
        "secondid": 100
      },
      {
        "base": "BNB",
        "quote": "BTC",
        "low": null,
        "high": null,
        "bid": null,
        "ask": null,
        "firstid": 410,
        "secondid": 100
      },
      {
        "base": "OMG",
        "quote": "BTC",
        "low": 3.572762E-4,
        "high": 3.96627E-4,
        "bid": 3.572762E-4,
        "ask": 3.96627E-4,
        "firstid": 381,
        "secondid": 100
      },
      {
        "base": "BAT",
        "quote": "BTC",
        "low": null,
        "high": null,
        "bid": null,
        "ask": null,
        "firstid": 358,
        "secondid": 100
      },
      {
        "base": "SEX",
        "quote": "BTC",
        "low": null,
        "high": 1.4331E-4,
        "bid": null,
        "ask": 1.4331E-4,
        "firstid": 1994,
        "secondid": 100
      },
      {
        "base": "DARK",
        "quote": "BTC",
        "low": 0.001,
        "high": 0.00115,
        "bid": 0.001,
        "ask": 0.00115,
        "firstid": 600,
        "secondid": 100
      },
      {
        "base": "433",
        "quote": "USD",
        "low": null,
        "high": 0.056,
        "bid": null,
        "ask": 0.056,
        "firstid": 2086,
        "secondid": 2
      },
      {
        "base": "USDT",
        "quote": "USD",
        "low": null,
        "high": 1,
        "bid": null,
        "ask": 1,
        "firstid": 258,
        "secondid": 2
      },
      {
        "base": "INTT",
        "quote": "USD",
        "low": null,
        "high": 1,
        "bid": null,
        "ask": 1,
        "firstid": 2090,
        "secondid": 2
      }
    ]
  `;
}