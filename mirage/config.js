import mobgetcurrenciesinfoi from './requests/mobgetcurrenciesinfoi';
import refreshPairData from './requests/refreshPairData';
import ticker from './requests/ticker';
import historyTradePair from './requests/historyTradePair';

import getUserHistory from './fixtures/history-user';
import getCashInfo from './fixtures/cash-info';
import orderbook from './fixtures/orderbook';

export default function() {

  // TODO: format sources files

  this.get('https://indacoin.com/api/uni/mobgetcurrenciesinfoi/1', () => {
    return mobgetcurrenciesinfoi();
  });

  this.post('https://indacoin.com/inner/RefreshPairDataNew', () => {
    return refreshPairData();
  });

  this.get('https://indacoin.com/api/ticker', () => {
    return ticker();
  });

  this.post('https://indacoin.com/transactions/GetUserHistory', () => {
    return getUserHistory;
  });

  this.post('https://indacoin.com/inner/HistoryTradePair', () => {
    return historyTradePair();
  });

  this.post('https://indacoin.com/change.aspx/getCashInfo', () => {
    return getCashInfo;
  });

  // TODO: different data
  this.get('https://indacoin.com/api/orderbook/:id', () => {
    return orderbook;
  });

  // TODO: random or list of currencies

  // https://indacoin.com/api/GetAltToAltMarketPrice/USD/BTC/1
  // https://indacoin.com/api/GetAltToAltMarketPrice/BTC/USD/0.000015801
  this.get('https://indacoin.com/api/GetAltToAltMarketPrice/:currencyFrom/:currencyTo/:amount', (schema, request) => {
    const currencyFrom = request.params.currencyFrom;
    const currencyTo = request.params.currencyTo;
    const amount = request.params.amount;
    const currencies =
    {
      'USD': 1,
      'EUR': 0.89,
      'RUB': 66.04,
      'AUD': 1.42,
      'GBP': 0.77
    };
    const cryptoCurrencies = {
      'BTC': 3789.74001,
      'ETH': 129.6896403172125,
      'XRP': 0.3038613540018,
      'LTC': 53.702397119504695,
      'EOS': 3.475570563171,
      'BCH': 126.12095584199581,
      'BNB': 14.127771783279,
      'USDT': 0.9821374607426859,
      'XLM': 0.1022850828699,
      'TRX': 0.021677312857200003,
      'ADA': 0.045438982719900003,
      'XMR': 47.6572312399533,
      'IOTA': 0.2715727691166,
      'DASH': 78.6948229478523,
      'NEO': 8.3843450033238,
      'ETC': 4.1158092404604,
      'XEM': 0.0431651387139,
      'ZEC': 48.26612876736,
      'ONT': 0.889830954348,
      'WAVES': 2.5921821668400002,
      'BAT': 0.1870994642937,
      'DOGE': 0.0018569726048999998,
      'OMG': 1.31125004346,
      'QTUM': 2.03130064536,
      'LINK': 0.46992776124,
      'STEEM': 0.495697993308,
      'DCR': 16.0096429800447,
      'ICX': 0.31676541873584996,
      'ZRX': 0.2507670964617,
      'ZIL': 0.017053830045,
      'LSK': 1.2290884800432,
      'DGB': 0.0120892706319,
      'BCN': 0.0006698365467674999,
      'BTS': 0.0451737009192,
      'NANO': 0.9152222124149999,
      'BCD': 0.71626086189,
      'AE': 0.419524219107,
      'SC': 0.0025012284066,
      'XVG': 0.0060256866159,
      'STRAT': 0.8540558086536,
      'SNT': 0.0211088518557,
      'ETN': 0.005371956464175,
      'WAX': 0.0477886215261,
      'NEXO': 0.07844003872698001,
      'QWARK': 0.0097017344256,
      'ACT': 0.010614682794009001,
      'CANN': 0.0052677386139,
      'OK': 0.013567269235800001,
      'YOYO': 0.0171296248452,
      'MCO': 2.74756150725,
      'USDC': 0,
      'MER': 0.0474854423253,
      'TAAS': 0.24946342589826,
      'SEQ': 0.009474350025000002,
      'XMY': 0.0012506142033,
      'PLBT': 1.05733746279,
      'B2G': 6.821532018,
      'CRW': 0.1136922003,
      'DLT': 0.095122474251,
      'VIB': 0.0255807450675,
      'ART': 0.019852174068384,
      'LIFE': 0.0000391859117034,
      'VET': 0.0044718932118,
      'EBST': 0.0166369586439,
      'SKY': 1.00428110265,
      'EMC2': 0.0801530012115,
      'GBG': 0.009474350025000002,
      'CVT': 0.005680062326988,
      'SYNX': 0.0236100802623,
      'NOAH': 0.0001469661175878,
      'FUN': 0.0039034322103,
      'UKG': 0.0302421252798,
      'BERRY': 0.0017436972760011,
      'FYP': 0.02652818007,
      'MTH': 0.018607623449099997,
      'CAS': 0.0066456880815360005,
      'WINGS': 0.0415355505096,
      'ZCL': 0.9969290070306001,
      'ENG': 0.3844312266144,
      'MDA': 1.0925820448830001,
      'WABI': 0.2007046309296,
      'DGD': 15.74636974155,
      'FLO': 0.061772762163,
      'NXT': 0.024633310064999997,
      'IOP': 0.1269183929349,
      'BRD': 0.217531076574,
      'EMC': 0.2775984557325,
      'AEON': 0.28233563074499995,
      'PTOY': 0.014481354526212,
      'SNC': 0.0183044442483,
      'BNK': 0.0017811778046999999,
      'BNT': 0.58361996154,
      'TKS': 0.30317920080000005,
      'MONA': 0.4739448856506,
      'RDD': 0.001136922003,
      'BCHABC': 126.03159377256,
      'FXT': 0.0018948700049999999,
      'DCN': 0.0000339560704896,
      'SEX': 0.27155382041655,
      '433': 0.028,
      'INTT': 0.028
    }

    if (currencyFrom in currencies)
      return (amount / currencies[currencyFrom]) / cryptoCurrencies[currencyTo];
    else
      return (amount * currencies[currencyTo]) * cryptoCurrencies[currencyFrom];
  });


  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */

  // this.urlPrefix = '';    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = '';    // make this `/api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
    Shorthand cheatsheet:

    this.get('/posts');
    this.post('/posts');
    this.get('/posts/:id');
    this.put('/posts/:id'); // or this.patch
    this.del('/posts/:id');

    http://www.ember-cli-mirage.com/docs/v0.4.x/shorthands/
  */
}
