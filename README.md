# Indaexchange

Indaexchange is written using Ember.js, Ember Fastboot, Bootstrap & Sass. This project also use Plotly.js for candlestick chart, BEM naming for CSS, Prettier for code formatting, ESLint for JS linting.

### Features

- Single account with cryptocurrency purchase website indacoin.com
- Transactions with your account: cryptocurrency deposit, funds withdrawal, buying cryptocurrency with a credit/debit card
- Personal account, transaction history and operations
- Trading pairs information, charts and order book
- Crypto trading: limit, market, stop-limit

## Prerequisites

You will need the following things properly installed on your computer:

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/)
* [Ember CLI](https://ember-cli.com/)


## Installation

* `git clone <repository-url>`
* `cd indaexchange/`
* `npm install`

## Running

### Development

* from the project folder `ember serve`
* visit your app at [http://localhost:4200/exchange/](http://localhost:4200/exchange/)

### Building

* `npm run build`

### Production

Frontend server is serviced on Node.js / Express.js that is remotely located on the Debian. After building, we get the assembled project folder "dist/". For daemonize Node.js server we use PM2:

* from the project folder `pm2 start server.js -i max --name "indaexchange"`
* stop/start/restart/delete/info `pm2 <stop/start/restart/delete/show> indaexchange`

## Further Reading / Useful Links

* [Ember.js](http://emberjs.com/)
* [Ember-cli](https://ember-cli.com/)
* [Plotly.js](https://plot.ly/)
* [BEM](https://bem.info/)
* [Ember Fastboot](https://www.ember-fastboot.com/)
* [PM2](https://pm2.keymetrics.io/)
* [Ember Inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
