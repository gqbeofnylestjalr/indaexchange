'use strict';

const DISTRIBUTIVE = 'dist';
const FastBootAppServer = require('fastboot-app-server');

let server = new FastBootAppServer({
    distPath: DISTRIBUTIVE,
    gzip: true,
    port: 4200
});
server.start();
console.log('Start server');
