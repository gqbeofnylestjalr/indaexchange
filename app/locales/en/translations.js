export default {
  "language-select":{
    "language":{
      "de": "German",
      'ar': 'Arabic',
      'en': 'English',
      'es': 'Spanish',
      "fr": "French",
      "it": "Italian",
      "pt": "Portuguese",
      'ru': 'Russian',
      'tr': 'Turkish'
    }
  },
  "header": {
    "orders": "Orders",
    "exchange": "Exchange",
    "trading": "Trading",
    "trades": "Trades",
    "funds": {
      "title": "Funds",
      "withdrawal": "Withdrawal",
      "deposit": "Deposit",
      "balance": "Balance"
    },
    "listing": "Listing"
  },
  "footer": {
    "news": "news",
    "partnership": "partnership",
    "questions": "questions",
    "terms-of-use": "terms of use",
    "change-region": "change region",
    "buy-links": {
      "link-1": "buy bitcoin",
      "link-2": "buy ethereum",
      "link-3": "buy ripple",
      "link-4": "buy litecoin",
      "link-5": "buy dash",
      "link-6": "buy bitcoin cash"
    }
  },
  "exchange": {
    "balance": "Balance",
    "top-markets": {
      "title": "Top markets",
      "market": "Market",
      "change": "Change",
      "total": "Total",
      "volume": "Volume",
      "high": "High",
      "low": "Low"
    },
    "asset-portfolio": {
      "title": "Asset portfolio",
      "coins": "Coins",
      "amount": "Amount",
      "frozen": "Frozen",
      "available": "Available",
      "market-value": "Market Value",
      "operation": "Operation"
    }
  },
  "funds": {
    "balance": {
      "exchange-rate": "Current exchange rate: {{{cryptocurrency}}}/USD={{{price}}}"
    },
    "deposit": {
      "payment-card": "Payment card",
      "bank-transfer": "Bank transfer",
      "cryptocurrency": "Cryptocurrency",
      "fee-message": "The fee is {{{feeFactPercent}}}% but not less than {{{minFee}}}. The minimum amount is {{{minSum}}}, maximum - is {{{maxSum}}}."
    },
    "history": {
      "title": "History"
    },
    "withdrawal": {
      "available-message": "Available: {{{available}}}",
      "fees-message": "Fees: {{{fees}}}",
      "correct-crypto-message": "Please enter correct crypto address",
      "modal": {
        "success-1": "Confirmation has been sent to your email address"
      }
    }
    //available-message', { available: available, fee: fee, cryptocurrency: cryptocurrency }));
  },
  // TODO: to funds
  "deposit": {
    "statuses": {
      "paid": "Paid",
      "not_paid": "Not paid",
      "verifying": "Verifying",
      "fail_verifying": "Declined",
      "Cancelled": "Canceled",
      "UserUnconfirmed": "Email confirmation",
      "UserConfirmed": "Queue",
      "BitcoinServerError": "Queue",
      "EKOServerError": "Queue",
      "LitecoinServerError": "Queue",
      "PMServerError": "Queue",
      "PayeerError": "Queue",
      "OKPayError": "Queue",
      "BTC2CCError": "Queue",
      "PaymentSystemError": "Queue",
      "AdminConfirmed": "In progress",
      "PaymentSystemConfirmed": "Completed",
      "MarginTrading": "Margin deal",
      "Completed": "Completed",
      "ManualConfirmed": "In progress",
      "not": "-"
    }
  },
  // 1:"Credit/Debit cards",2:"QIWI terminals",3:"QIWI",4:"Bashkomsnabbank",5:"Novoplat terminals",6:"Elexnet terminals",7:"Bank cards",8:"Alfa-click",9:"Wire transfer",10:"Bitcoin",12:"Litecoin",13:"Astropay",15:"QIWI",16:"Credit/Debit cards(USD)",17:"Pinpay",18:"Payeer",19:"Liqpay",20:"Yandex.Money",21:"Elexnet",22:"Kassira.net",23:"Mobil element",24:"Svyaznoy",25:"Euroset",26:"PerfectMoney",27:"IndacoinInternal",29:"Yandex.Money",30:"OKPay",31:"Affilate program",32:"Investments",33:"Payza",34:"Credit/Debit cards",35:"BTC-e Code",36:"Credit/Debit cards",37:"Credit/Debit cards",39:"International Bank Transfer",40:"Yandex.Money",41:"Interest charge",42:"QIWI(Handmade)",43:"UnionPay",44:"QIWI(Automatic)",45:"Mobile commerce(Russia only)",50:"Credit/Debit cards(EUR)",51:"QIWI(Automatic)",53:"Ethereum",54:"Credit/Debit cards"}
  "cash-in": {
    "1": "USD",
    "2": "e-Payment",
    "3": "QIWI",
    "4": "Bashcomsnabbank",
    "5": "Novoplat terminals",
    "6": "Elecsnet terminals",
    "7": "USD",
    "8": "Alpha Click",
    "9": "International wire transfer",
    "10": "Bitcoin",
    "12": "Litecoin",
    "13": "Astropay",
    "15": "QIWI",
    "16": "USD",
    "17": "Pinpay",
    "18": "Payeer",
    "19": "Liqpay",
    "20": "Yandex.Money",
    "21": "Elecsnet",
    "22": "Kassira.net",
    "23": "Mobile Element",
    "24": "Svyaznoy",
    "25": "Euroset",
    "26": "PerfectMoney",
    "27": "Indacoin — internal",
    "28":	"CouponBonus",
    "29": "Yandex.Money",
    "30": "OKPay",
    "31": "Partner Program",
    "33": "Payza",
    "35": "BTC-E code",
    "36": "USD",
    "37": "USD",
    "38": "Altcoins",
    "39": "International wire transfer",
    "40": "Yandex.Money",
    "41": "Interest charge",
    "42": "QIWI(Manual)",
    "43": "UnionPay",
    "44": "QIWI(Automatic)",
    "45": "Pay by phone",
    "49": "LibrexCoin",
    "50": "EURO",
    "51": "QIWI(Automatic)",
    "52": "Pay by phone",
    "53": "Ethereum",
    "54": "RUB",
    "55": "AUD",
    "56": "GBP",
    "not": "-"
  }
}