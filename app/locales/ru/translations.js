export default {
  "language-select":{
    "language":{
      "de": "German",
      'ar': 'Arabic',
      'en': 'English',
      'es': 'Spanish',
      "fr": "French",
      "it": "Italian",
      "pt": "Portuguese",
      'ru': 'Russian',
      'tr': 'Turkish'
    }
  },
  "header": {
    "orders": "Заказы",
    "exchange": "Обмен",
    "funds": {
      "title": "Средства",
      "withdrawal": "Withdrawal",
      "deposit": "Deposit",
      "balance": "Balance"
    },
    "listing": "Список"
  },
  "footer": {
    "news": "Новости",
    "partnership": "Партнерство",
    "questions": "Вопросы",
    "terms-of-use": "Правила пользования",
    "change-region": "Сменить регион",
    "buy-links": {
      "link-1": "Купить bitcoin",
      "link-2": "Купить ethereum",
      "link-3": "Купить ripple",
      "link-4": "Купить litecoin",
      "link-5": "Купить dash",
      "link-6": "Купить bitcoin cash"
    }
  },
  "exchange": {
    "balance": "Balance",
    "top-markets": {
      "title": "Top markets",
      "market": "Market",
      "change": "Change",
      "total": "Total",
      "volume": "Volume",
      "high": "High",
      "low": "Low"
    },
    "asset-portfolio": {
      "title": "Asset portfolio",
      "coins": "Coins",
      "amount": "Amount",
      "frozen": "Frozen",
      "available": "Available",
      "market-value": "Market Value",
      "operation": "Operation"
    }
  },
  "funds": {
    "deposit": {
      "payment-card": "Кредитная карта",
      "bank-transfer": "Банковский перевод",
      "cryptocurrency": "Криптовалюта"
    },
    "history": {
      "title": "History"
    }
  },
  "deposit": {
    "statuses": {
      "paid": "Paid",
      "not_paid": "Not paid",
      "verifying": "Verifying",
      "fail_verifying": "Declined",
      "Cancelled": "Canceled",
      "UserUnconfirmed": "Email confirmation",
      "UserConfirmed": "Queue",
      "BitcoinServerError": "Queue",
      "EKOServerError": "Queue",
      "LitecoinServerError": "Queue",
      "PMServerError": "Queue",
      "PayeerError": "Queue",
      "OKPayError": "Queue",
      "BTC2CCError": "Queue",
      "PaymentSystemError": "Queue",
      "AdminConfirmed": "In progress",
      "PaymentSystemConfirmed": "Completed",
      "MarginTrading": "Margin deal",
      "Completed": "Completed",
      "ManualConfirmed": "In progress"
    }
  },
  "cash-in": {
    "1": "USD",
    "2": "e-Payment",
    "3": "QIWI",
    "4": "Bashcomsnabbank",
    "5": "Novoplat terminals",
    "6": "Elecsnet terminals",
    "7": "USD",
    "8": "Alpha Click",
    "9": "International wire transfer",
    "10": "Bitcoin",
    "12": "Litecoin",
    "13": "Astropay",
    "15": "QIWI",
    "16": "USD",
    "17": "Pinpay",
    "18": "Payeer",
    "19": "Liqpay",
    "20": "Yandex.Money",
    "21": "Elecsnet",
    "22": "Kassira.net",
    "23": "Mobile Element",
    "24": "Svyaznoy",
    "25": "Euroset",
    "26": "PerfectMoney",
    "27": "Indacoin — internal",
    "28":	"CouponBonus",
    "29": "Yandex.Money",
    "30": "OKPay",
    "31": "Partner Program",
    "33": "Payza",
    "35": "BTC-E code",
    "36": "USD",
    "37": "USD",
    "39": "International wire transfer",
    "40": "Yandex.Money",
    "42": "QIWI(Manual)",
    "43": "UnionPay",
    "44": "QIWI(Automatic)",
    "45": "Pay by phone",
    "49": "LibrexCoin",
    "50": "EURO",
    "51": "QIWI(Automatic)",
    "52": "Pay by phone",
    "53": "Ethereum",
    "54": "RUB",
    "55": "AUD",
    "56": "GBP"
  }
}