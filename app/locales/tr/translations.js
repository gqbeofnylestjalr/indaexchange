export default {
    "language-select":{
      "language":{
        "de": "German",
        'ar': 'Arabic',
        'en': 'English',
        'es': 'Spanish',
        "fr": "French",
        "it": "Italian",
        "pt": "Portuguese",
        'ru': 'Russian',
        'tr': 'Turkish'
      }
    }
  }