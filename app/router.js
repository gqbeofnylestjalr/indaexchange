import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  // TODO: root http://localhost:4200/exchange not server, only http://localhost:4200/exchange/
  //this.route('index', {path: '/'});
  this.route('inner'); // TODO: delete route, all data and css
  this.route('orders'); // TODO: delete route, all data and css
  this.route('listing'); // TODO: delete route, all data and css

  this.route('funds', function() {
    this.route('withdrawal');
    this.route('deposit');
    this.route('balance');
  });

  this.route('cards');
  this.route('trades');
  this.route('login');
  this.route('trading');
  this.route('exchange');
});

export default Router;
