import DS from 'ember-data';
import fetch from 'fetch';

export default DS.RESTAdapter.extend({
  query(store, type, query) {
    const pair = query.pairName;
    const count = query.count;
    const url = `https://indacoin.com/inner/HistoryTradePair`;
    const data = JSON.stringify({
      pair: pair,
      count: count,
    });
    return fetch(url, {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        accept: '*/*',
        'x-requested-with': 'XMLHttpRequest',
        origin: 'https://indacoin.com',
      },
      body: data,
    })
      .then(function(response) {
        return response.json();
      })
      .then(data => {
        return data.d;
      });
  },
});
