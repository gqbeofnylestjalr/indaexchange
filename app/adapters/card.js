import DS from 'ember-data';
import fetch from 'fetch';

export default DS.JSONAPIAdapter.extend({
  query(store, type, query) {
    const url = `https://indacoin.com/funding/GetMyCards`;
    return fetch(url, {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        accept: '*/*',
        'x-requested-with': 'XMLHttpRequest',
        origin: 'https://indacoin.com',
      },
    })
      .then(function(response) {
        return response.json();
      })
      .then(data => {
        return data.d;
      });
  },
});
