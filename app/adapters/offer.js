import DS from 'ember-data';
import fetch from 'fetch';

export default DS.RESTAdapter.extend({
  query(store, type, query) {
    let pairName = query.pairName;
    return fetch(`https://indacoin.com/api/orderbook/${pairName}`)
      .then(data => {
        return data.json();
      })
      .then(data => {
        data.pairName = pairName;
        return data;
      });
  },

  //shouldReloadRecord: function(store, snapshot) { return false; },
  //shouldBackgroundReloadRecord: function(store, snapshot) { return false; },
  //shouldReloadAll: function(store, snapshot) { return false; },
  //shouldBackgroundReloadAll: function(store, snapshot) { return false; }
});
