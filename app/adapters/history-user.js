import DS from 'ember-data';
import fetch from 'fetch';

export default DS.RESTAdapter.extend({
  query(store, type, query) {
    const filter = query.operationFilter;
    const url = `https://indacoin.com/transactions/GetUserHistory`;
    // TODO: актуальную DateTo и другие параметры
    const data = JSON.stringify({
      columnName: 'date',
      dateFrom: '01-05-2014',
      dateTo: '09-11-2020',
      operationFilter: filter, // 143 - deposit, 2 - withdrawal, 1 - new deposit
      rowFrom: 0,
      sortOrder: 1,
    });
    return fetch(url, {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        accept: '*/*',
        'x-requested-with': 'XMLHttpRequest',
        origin: 'https://indacoin.com',
      },
      body: data,
    })
      .then(function(response) {
        return response.text();
      })
      .then(data => {
        return data;
      });
  },
});
