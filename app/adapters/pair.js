import DS from 'ember-data';
import fetch from 'fetch';

export default DS.RESTAdapter.extend({
  query(store, type, query) {
    const pairName = query.pairName;
    const url = `https://indacoin.com/inner/RefreshPairDataNew`;
    return fetch(url, {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        accept: '*/*',
        'x-requested-with': 'XMLHttpRequest',
        origin: 'https://indacoin.com',
      },
      body: `{'pair':'${pairName}'}`,
    }).then(function(response) {
      return response.json();
    });
  },
});
