import DS from 'ember-data';
//import CachedShoe from 'ember-cached-shoe';

export default DS.JSONAPIAdapter.extend(
  /*CachedShoe,*/ {
    shouldBackgroundReloadAll: function(store, snapshot) {
      return false;
    },

    host: 'https://indacoin.com',
    namespace: 'api/uni',

    pathForType() {
      return 'mobgetcurrenciesinfoi/1';
    },
  },
);
