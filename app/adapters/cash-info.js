import DS from 'ember-data';
import fetch from 'fetch';

export default DS.RESTAdapter.extend({
  query(store, type, query) {
    const url = `https://indacoin.com/change.aspx/getCashInfo`;
    return fetch(url, {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        accept: '*/*',
        'x-requested-with': 'XMLHttpRequest',
        origin: 'https://indacoin.com',
        //"content-type": "application/json; charset=utf-8",
        //"accept": "application/json, text/javascript, */*; q=0.01",
      },
    }).then(function(response) {
      return response.json();
    });
  },
});
