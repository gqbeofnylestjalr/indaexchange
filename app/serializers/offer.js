import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    let json = this._super(...arguments);
    var id = 1;
    let swap = [];
    if (payload.bid)
      payload.bid.forEach(item => {
        let obj = {};
        obj.id = id++;
        obj.type = 'offer';
        obj.attributes = {
          pair: payload.pairName,
          type: 'bid',
          price: toFixed(item.price),
          base_amount: item.base_amount,
          quote_amount: toFixed(item.quote_amount),
          fee_amount: toFixed(item.fee_amount),
        };
        delete item.price;
        delete item.base_amount;
        delete item.quote_amount;
        delete item.fee_amount;
        // TODO: delete payload.bid/ask
        swap.push(obj);
      });

    if (payload.ask)
      payload.ask.forEach(item => {
        let obj = {};
        obj.id = id++;
        obj.type = 'offer';
        obj.attributes = {
          pair: payload.pairName,
          type: 'ask',
          price: item.price,
          base_amount: item.base_amount,
          quote_amount: item.quote_amount,
          fee_amount: item.fee_amount,
        };
        delete item.price;
        delete item.base_amount;
        delete item.quote_amount;
        delete item.fee_amount;
        // TODO: delete payload.bid/ask
        swap.push(obj);
      });
    json.data = swap;
    return json;
  },
});

/*
[{"price":0.0010000000
 1e-7
*/

// TODO: rewrite to hepler and use not here

// function toFixed(x) {
//   if (Math.abs(x) < 1.0) {
//     var e = parseInt(x.toString().split('e-')[1]);
//     if (e) {
//         x *= Math.pow(10,e-1);
//         x = '0.' + (new Array(e)).join('0') + x.toString().substring(2);
//     }
//   } else {
//     var e = parseInt(x.toString().split('+')[1]);
//     if (e > 20) {
//         e -= 20;
//         x /= Math.pow(10,e);
//         x += (new Array(e+1)).join('0');
//     }
//   }
//   return x;
// }

function toFixed(x) {
  return Number(x)
    .toFixed(15)
    .replace(/\.?0+$/, '');
}
