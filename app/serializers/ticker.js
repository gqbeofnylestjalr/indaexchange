import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    let json = this._super(...arguments);

    payload.forEach(item => {
      item.id = `${item.base}_${item.quote}`;
      item.type = 'ticker';
      item.attributes = {
        base: item.base,
        quote: item.quote,
        low: item.low,
        high: item.high, // BTC_USD покупаю BTC за USD / base and quote
        firstid: item.firstid,
        secondid: item.secondid,
      };
      delete item.base;
      delete item.quote;
      delete item.low;
      delete item.high;
      delete item.firstid;
      delete item.secondid;
    });
    json.data = payload;
    return json;
  },
});
