import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    let json = this._super(...arguments);

    var cashIn = JSON.parse(payload.d).cashTypes.cashIn;
    var data = [];

    // for (let item in cashIn) {
    //   console.log(cashIn.cashTypes.cashIn[item].fee.feePercent)
    // }

    for (let item in cashIn) {
      let obj = {};
      obj.id = cashIn[item].factCurrencyId;
      obj.type = 'cash-info';
      obj.attributes = {
        typeId: cashIn[item].typeId,
        factCurrencyId: cashIn[item].factCurrencyId,
        minSum: cashIn[item].fee.minSum,
        maxSum: cashIn[item].fee.maxSum,
        feeFactPercent: cashIn[item].fee.feeFactPercent,
        minFee: cashIn[item].fee.minFee,
      };
      data.push(obj);
    }
    json.data = data;
    return json;

    /*"{"cashTypes":{
      "cashIn":{
        "50":{
          "typeId":50,
          "currencyId":"EUR",
          "currencyIdInt":7,
          "factCurrencyId":"EUR",
          "fields":{"name":"QuickPay","fields":[{"name":"amount","params":{"currency":"USD"}},{"name":"commonFee"},{"name":"phone"},{"name":"fio","type":"input","params":{"regexp":".+"}},{"name":"bday","type":"input","params":{"regexp":"^[0-9]{2}_[0-9]{2}_[0-9]{4}$","mask":"**_**_****","mask_placeholder":"MM_DD_YYYY"}}]},
          "fee":{
            "minSum":30.00000000,
            "maxSum":3000.00000000,
            "reserv":0.0,
            "minFee":2.700000000,
            "feeAdd":0.00000000,
            "feePercent":0.09000,
            "feeFactPercent":0.09000,
            "feeFactAdd":0.25000000,
            "feeFactExtraPercent":0.00000000,
            "feeFactExtraAdd":0.00000000,
            "defaultValue":30.00000000},
            "fullName":"Credit Cards QuickPay",
            "isCrypto":0,
            "execution":"0",
            "shortcode":"CARDEUR",
            "commentPath":"CashTypes.Deposit.MasterCardProblem",
            "limitDay":null,
            "limitWeek":null,
            "limitMonth":null},
        "54":{
          "typeId":54,
          "currencyId":"USD",
          "currencyIdInt":4,
          "factCurrencyId":"RUB","fie*/

    // debugger;
    //     let usdAvailable = (!!obj.cashTypes.cashIn['16']) || false;
    //     let eurAvailable = (!!obj.cashTypes.cashIn['50']) || false;
    //     let rubAvailable = (!!obj.cashTypes.cashIn['54']) || false;
    //     let btcAvailable = (!!obj.cashTypes.cashIn['10']) || false;

    //     let audAvailable = (!!obj.cashTypes.cashIn['55']) || false;
    //     let gbpAvailable = (!!obj.cashTypes.cashIn['56']) || false;

    //     let usdmin = 30.00000000;
    //     if (obj.cashTypes.cashIn['16'])
    //       obj.cashTypes.cashIn['16'].fee.minSum;

    //     let usdmax = 3000.00000000;
    //     if (obj.cashTypes.cashIn['16'])
    //       obj.cashTypes.cashIn['16'].fee.maxSum;

    //     let eurmin = 30.00000000;
    //     if (obj.cashTypes.cashIn['50'])
    //       obj.cashTypes.cashIn['50'].fee.minSum;

    //     let eurmax = 3000.00000000;
    //     if (obj.cashTypes.cashIn['50'])
    //       obj.cashTypes.cashIn['50'].fee.maxSum;

    //     let rubmin = 2000.00000000;
    //     if (obj.cashTypes.cashIn['54'])
    //       obj.cashTypes.cashIn['54'].fee.minSum;

    //     let rubmax = 200000.00000000;
    //     if (obj.cashTypes.cashIn['54'])
    //       obj.cashTypes.cashIn['54'].fee.maxSum;

    //     let btcmin = 0.01;
    //     let btcmax = 0.2;

    //     let audmin = 30.00000000;
    //     if (obj.cashTypes.cashIn['55'])
    //       obj.cashTypes.cashIn['55'].fee.minSum;

    //     let audmax = 30000.00000000;
    //     if (obj.cashTypes.cashIn['55'])
    //       obj.cashTypes.cashIn['55'].fee.maxSum;

    //     let gbpmin = 30.00000000;
    //     if (obj.cashTypes.cashIn['56'])
    //       obj.cashTypes.cashIn['56'].fee.minSum;

    //     let gbpmax = 3000.00000000;
    //     if (obj.cashTypes.cashIn['56'])
    //       obj.cashTypes.cashIn['56'].fee.maxSum;

    //     // False in case if no one currency is available
    //     // true - show
    //     let changeDirection = !(usdAvailable || eurAvailable || rubAvailable || btcAvailable || audAvailable || gbpAvailable); // show Alert
    // debugger;
    //     let data =
    //       [{
    //         "id": "1",
    //         "type": "cash-info",
    //         "attributes": {

    //           /* The fee is 9.0% but not less than 2.7$ . The minimum amount is 30$.
    //           fee.feePercent = 0.09
    //           fee.minFee = 2.7
    //           fee.minSum = 30

    //           */

    //           // "minFee": obj.fee.minFee,
    //           // "feePercent": obj.fee.feePercent,

    //           "usdAvailable": usdAvailable,
    //           "eurAvailable": eurAvailable,
    //           "rubAvailable": rubAvailable,
    //           "btcAvailable": btcAvailable,
    //           "audAvailable": audAvailable,
    //           "gbpAvailable": gbpAvailable,
    //           "usdmin": usdmin,
    //           "usdmax": usdmax,
    //           "eurmin": eurmin,
    //           "eurmax": eurmax,
    //           "rubmin": rubmin,
    //           "rubmax": rubmax,
    //           "btcmin": btcmin,
    //           "btcmax": btcmax,
    //           "audmin": audmin,
    //           "audmax": audmax,
    //           "gbpmin": gbpmin,
    //           "gbpmax": gbpmax,
    //           "changeDirection": changeDirection,

    //           "usdCommentPath": obj.cashTypes.cashIn['16'].commentPath,
    //           "eurCommentPath": obj.cashTypes.cashIn['50'].commentPath,
    //           "rubCommentPath": obj.cashTypes.cashIn['54'].commentPath,
    //           "btcCommentPath": obj.cashTypes.cashIn['10'].commentPath,
    //           "audCommentPath": obj.cashTypes.cashIn['55'].commentPath,
    //           "gbpCommentPath": obj.cashTypes.cashIn['56'].commentPath

    //         }
    //       }];
    //     json.data = data;
    //     return json;
  },
});
