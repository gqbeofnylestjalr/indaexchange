import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    let json = this._super(...arguments);

    var id = 1;
    payload.forEach(item => {
      item.id = id++;
      item.type = 'history-trade';
      item.attributes = {
        base: toFixed(item.base),
        quote: toFixed(item.quote),
        price: toFixed(item.price),
        timestamp: item.timestamp,
      };
      delete item.base;
      delete item.quote;
      delete item.price;
      delete item.timestamp;
    });
    json.data = payload;
    return json;
  },
});

function toFixed(x) {
  return Number(x)
    .toFixed(15)
    .replace(/\.?0+$/, '');
}
