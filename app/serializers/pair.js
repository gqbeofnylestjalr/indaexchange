import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    let json = this._super(...arguments);

    let included = [];
    let orders = [];
    let graph = [];
    let balance = [];
    let cryptoAddresses = [];
    let id = 1;

    let btcAddr_v = payload.d.btcAddr.v;
    let btcAddr_h = payload.d.btcAddr.h;

    let ethAddr_v = payload.d.ethAddr.v;
    let ethAddr_h = payload.d.ethAddr.h;

    let ltcAddr_v = payload.d.ltcAddr.v;
    let ltcAddr_h = payload.d.ltcAddr.h;

    let lxcAddr_v = payload.d.lxcAddr.v;
    let lxcAddr_h = payload.d.lxcAddr.h;

    included.push(
      JSON.parse(
        `{
        "id": "BTC",
        "type": "crypto-address",
        "attributes": {
          "wallet": "${btcAddr_v}",
          "hash": "${btcAddr_h}"
        }
      }`,
      ),
    );
    included.push(
      JSON.parse(
        `{
        "id": "ETH",
        "type": "crypto-address",
        "attributes": {
          "wallet": "${ethAddr_v}",
          "hash": "${ethAddr_h}"
        }
      }`,
      ),
    );
    included.push(
      JSON.parse(
        `{
        "id": "LTC",
        "type": "crypto-address",
        "attributes": {
          "wallet": "${ltcAddr_v}",
          "hash": "${ltcAddr_h}"
        }
      }`,
      ),
    );
    included.push(
      JSON.parse(
        `{
        "id": "LXC",
        "type": "crypto-address",
        "attributes": {
          "wallet": "${lxcAddr_v}",
          "hash": "${lxcAddr_h}"
        }
      }`,
      ),
    );

    cryptoAddresses.push(
      `{
        "id": "BTC",
        "type": "crypto-address"
      }`,
    );
    cryptoAddresses.push(
      `{
        "id": "ETH",
        "type": "crypto-address"
      }`,
    );
    cryptoAddresses.push(
      `{
        "id": "LTC",
        "type": "crypto-address"
      }`,
    );
    cryptoAddresses.push(
      `{
        "id": "LXC",
        "type": "crypto-address"
      }`,
    );

    // {
    //   "Id":1260,
    //   "CI":410,
    //   "pair":"BNB_BTC",
    //   "C":3.17000000,
    //   "P":0.0270000000,
    //   "A":0.08559000,
    //   "F":0.00000856,
    //   "B":true,
    //   "DT":1542806752
    //   }
    payload.d.userOrdersData.forEach(item => {
      let obj = {};
      obj.id = item.Id;
      obj.type = 'current-order';
      obj.attributes = {
        orderId: item.Id,
        pair: item.pair,
        time: item.DT,
        side: item.B,
        orderPrice: toFixed(item.P),
        orderAmount: item.A,
        unexecuted: toFixed(item.C),
        executed: toFixed(item.F),
      };
      //included.push(obj);
      included.unshift(obj);

      let order = {};
      order.type = 'current-order';
      order.id = item.id;
      //orders.push(order);
      orders.unshift(order);
    });

    payload.d.graphData.forEach(item => {
      let obj = {};
      obj.id = id;
      obj.type = 'graph';
      obj.attributes = {
        max: item.max,
        min: item.min,
        open: item.open,
        close: item.close,
        volume: item.volume,
        timestamp: item.timestamp,
      };
      if (String(obj.attributes.timestamp).length === 10)
        obj.attributes.timestamp = Number(obj.attributes.timestamp + '000');
      included.push(obj);

      let graphMeta = {};
      graphMeta.type = 'graph';
      graphMeta.id = id++;
      graph.push(graphMeta);
    });

    id = 1;
    for (let item in payload.d.balance) {
      let obj = {};
      obj.id = item;
      obj.type = 'balance';
      obj.attributes = {
        currency: item,
        amount: payload.d.balance[item],
      };
      included.push(obj);

      let balanceMeta = {};
      balanceMeta.type = 'balance';
      balanceMeta.id = id++;
      balance.push(balanceMeta);
    }

    let data = [
      {
        id: '1',
        type: 'pair',
        attributes: {
          userName: payload.d.accData.Nick,
        },
        relationships: {
          orders: orders,
          graph: graph,
        },
      },
    ];

    json.data = data;
    json.included = included;
    return json;
  },
});

// TODO: rewrite to hepler and use not here
// function toFixed(x) {
//   if (Math.abs(x) < 1.0) {
//     var e = parseInt(x.toString().split('e-')[1]);
//     if (e) {
//         x *= Math.pow(10,e-1);
//         x = '0.' + (new Array(e)).join('0') + x.toString().substring(2);
//     }
//   } else {
//     var e = parseInt(x.toString().split('+')[1]);
//     if (e > 20) {
//         e -= 20;
//         x /= Math.pow(10,e);
//         x += (new Array(e+1)).join('0');
//     }
//   }
//   return x;
// }

function toFixed(x) {
  return Number(x)
    .toFixed(15)
    .replace(/\.?0+$/, '');
}
