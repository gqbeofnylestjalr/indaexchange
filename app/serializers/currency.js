import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let json = this._super(...arguments);
    var payload2 = [];
    payload.result.forEach(function(item) {
      payload2.push(item);
    });
    payload2.shift();
    payload2.shift();
    payload2.shift();
    payload2.shift();
    payload2.shift();

    payload = payload2;

    // payload.slice().reverse().forEach(function(item, index, object) {
    //   if (item.isActive == false) {
    //     payload.splice(object.length - 1 - index, 1);
    //   }
    // });

    payload.forEach((currency, index, object) => {
      currency.id = currency.short_name;
      currency.type = 'currency';
      let name = currency.name;
      if (name === 'I/O Coin') name = 'IO Coin';

      currency.attributes = {
        cur_id: currency.cur_id,
        ext_market: currency.ext_market,
        txfee: currency.txfee,
        short_name: currency.short_name,
        name: name,
        cointype: currency.cointype,
        imageurl: currency.imageurl,
        availableSupply: currency.availableSupply,
        price: currency.price,
        tag: currency.tag,
        isActive: currency.isActive,
        withdrawEnabled: currency.withdrawEnabled,

        changeDay: currency.changes.usd['24h'],
        changeWeek: currency.changes.usd['1w'],
        changeMonth: currency.changes.usd['1m'],
      };

      // TODO: delete all fields
      delete currency.cur_id;
      delete currency.ext_market;
      delete currency.txfee;
      delete currency.short_name;
      delete currency.name;
      delete currency.cointype;
      delete currency.imageurl;
      delete currency.availableSupply;
      delete currency.price;
      delete currency.isActive;
      delete currency.withdrawEnabled;
      delete currency.tag;
    });

    let swap = [];

    for (var i = 0; i < 6; i++) {
      let obj = {};
      obj.id = payload[i].id;
      obj.type = payload[i].type;
      obj.attributes = {
        cur_id: payload[i].attributes.cur_id,
        ext_market: payload[i].attributes.ext_market,
        txfee: payload[i].attributes.txfee,
        short_name: payload[i].attributes.short_name,
        name: payload[i].attributes.name,
        cointype: payload[i].attributes.cointype,
        imageurl: payload[i].attributes.imageurl,
        availableSupply: payload[i].attributes.availableSupply,
        price: payload[i].attributes.price,
        tag: payload[i].attributes.tag,
        isActive: payload[i].attributes.isActive,
        withdrawEnabled: payload[i].attributes.withdrawEnabled,

        changeDay: payload[i].attributes.changeDay,
        changeWeek: payload[i].attributes.changeWeek,
        changeMonth: payload[i].attributes.changeMonth,
      };
      swap.push(obj);
    }

    payload = payload.sort(function(a, b) {
      var nameA = a.attributes.short_name.toUpperCase();
      var nameB = b.attributes.short_name.toUpperCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });

    payload = swap.concat(payload);

    // var i = 0;
    // payload.forEach(function(item){
    //   item.id = i;
    //   i++;
    // });

    json.data = payload;
    return json;
  },
});
