import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    let json = this._super(...arguments);

    var payload2 = JSON.parse(JSON.parse(payload).d);

    payload2.forEach(item => {
      let operation_type = item.type;
      item.type = 'history-user';
      item.attributes = {
        txid: item.id,
        date: timeConverter(item.date),
        operation_type: operation_type,
        status: item.addInfo.status,
        is_crypto: item.addInfo.is_crypto,
        add_info: item.addInfo.add_info,
        price: item.price,
        amount_cur: item.amount_cur,
        total_cur: item.total_cur,
        total: item.total,
        fee_cur: item.fee_cur,
        fee: item.fee,
        amount: item.amount,
        typeId: item.addInfo.typeId,
      };

      // TODO: statuses case "status_var1.png" and "status_var2.png". Merge td columns
      if (!item.addInfo.status || item.addInfo.status === '0') item.attributes.status = 'not';
      if (!item.addInfo.typeId) item.attributes.typeId = 'not';

      delete item.date;
      delete item.addInfo.status;
      delete item.addInfo.is_crypto;
      delete item.addInfo.add_info;
      delete item.price;
      delete item.amount_cur;
      delete item.total_cur;
      delete item.total;
      delete item.fee_cur;
      delete item.fee;
      delete item.amount;
      delete item.addInfo.typeId;
    });
    json.data = payload2;
    return json;
  },
});

function timeConverter(date) {
  let a = new Date(date);
  let year = a.getFullYear();
  let month = ('0' + (a.getMonth() + 1)).slice(-2);
  let day = ('0' + a.getDate()).slice(-2);
  let hour = ('0' + a.getHours()).slice(-2);
  let min = ('0' + a.getMinutes()).slice(-2);
  return day + '.' + month + '.' + year + ' ' + hour + ':' + min;
}
