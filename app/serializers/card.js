import DS from 'ember-data';
import ENV from 'indaexchange/config/environment';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    let json = this._super(...arguments);
    let swap = [];
    payload.cards.forEach(item => {
      let obj = {};
      obj.type = 'card';
      obj.id = item.card_id;
      obj.attributes = {
        card_id: item.card_id,
        created_at: timeConverter(item.created_at.slice(6, 19)), // TODO: regular
        //created_at: '1482246364163',
        card_type: item.card_type,
        card_number: item.card_number,
        authorization_code: item.authorization_code,
        limit_remain: item.limit_remain,
        status_random_sum_string: item.status_random_sum_string,
        attempts_random_sum: item.attempts_random_sum,
        status_authorization_code_string: item.status_authorization_code_string,
        attempts_auth_code: item.attempts_auth_code,
        status_string: item.status_string,
      };
      delete item.card_type;
      delete item.card_number;
      delete item.authorization_code;
      delete item.limit_remain;
      delete item.status_random_sum_string;
      delete item.attempts_random_sum;
      delete item.status_authorization_code_string;
      delete item.attempts_auth_code;
      delete item.status_string;
      delete item.card_id;
      delete item.created_at;
      // var str = "/Date(1482246364163)/";
      // var result = str.match( /\/Date\((.+)\)\//g );
      // console.log( result ); // javascript (всё совпадение полностью)

      swap.push(obj);
    });

    if (ENV.environment === 'development') {
      // TODO: delete mockup and make normal mirage
      let included = [];
      included.push(
        JSON.parse(
          `{
          "id": "1",
          "type": "phone",
          "attributes": {
            "phoneId": "143603",
            "phoneAuthorizationCode": "2***",
            "phoneNumber": "+********66",
            "phoneStatusAuthorizationCode": "Verifying",
            "attemptsAuthorizationCode": "2"
          }
        }`,
        ),
      );
      json.included = included;
    } else {
      if (payload.phone) {
        let included = [];
        included.push(
          JSON.parse(
            `{
            "id": "BTC",
            "type": "phone",
            "attributes": {
              "phoneId": "${payload.phone[0].phone_id}",
              "phoneAuthorizationCode": "${payload.phone[0].phone_authorization_code}",
              "phoneNumber": "${payload.phone[0].phone_number}",
              "phoneStatusAuthorizationCode": "${
                payload.phone[0].phone_status_authorization_code_string
              }",
              "attemptsAuthorizationCode": "${payload.phone[0].attempts_authorization_code}"
            }
          }`,
          ),
        );
        json.included = included;
      }
    }

    json.data = swap;
    return json;
  },
});

function timeConverter(date) {
  let a = new Date(Number(date));
  let year = a.getFullYear();
  let month = ('0' + (a.getMonth() + 1)).slice(-2);
  let day = ('0' + a.getDate()).slice(-2);
  let hour = ('0' + a.getHours()).slice(-2);
  let min = ('0' + a.getMinutes()).slice(-2);
  return day + '.' + month + '.' + year + ' ' + hour + ':' + min;
}
