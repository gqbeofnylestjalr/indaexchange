import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  currency: "BTC",

  currentlyLoading: computed('model.pair', function() {
    return Boolean(!this.get('model.pair'));
  }),

  qrCodeLink: computed("wallet", "hash", function () {
    const wallet = this.get("crypto-address").get("wallet");
    const hash  = this.get("crypto-address").get("hash");
    return `https://indacoin.com/tools/ImageProxy.ashx?hash=${hash}&imgurl=http://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=${wallet}`;
  }),
	"crypto-address": computed('currency', function(){
		return this.store.peekRecord('crypto-address', this.get('currency'));
  })

});
