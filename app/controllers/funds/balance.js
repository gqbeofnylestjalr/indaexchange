import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend({
  i18n: service(),
  store: service(),

  cryptocurrency: 'BTC',

  cryptocurrencyList: computed('balance', function() {
    let result = [];
    this.get('balance').forEach(function(item) {
      result.push(item.get('currency'));
    });
    return result;
  }),

  currentlyLoading: computed('model.pair', function() {
    //return false;
    return Boolean(!this.get('model.pair'));
  }),

  exchangeRate: computed('cryptocurrency', function() {
    const i18n = this.get('i18n');
    const cryptocurrency = this.get('cryptocurrency');
    const record = this.get('store').peekRecord('currency', cryptocurrency);
    const price = record.get('price.usd'); // 6301.34
    return Ember.String.htmlSafe(i18n.t('funds.balance.exchange-rate', { cryptocurrency: cryptocurrency, price: price }));
  }),

  totalPrice: computed('cryptocurrency', function() {
    const i18n = this.get('i18n');
    const cryptocurrency = this.get('cryptocurrency');
    const cryptocurrencyRecord = this.get('store').peekRecord('currency', cryptocurrency);
    const balanceRecord = this.get('store').peekRecord('balance', cryptocurrency);
    const price = cryptocurrencyRecord.get('price.usd'); // 6301.34
    const amount = balanceRecord.get('amount'); // 0.031
    const total = (price * amount).toFixed(2);
    return `${total} USD ≈ ${amount}`;
  }),

  actions: {
    switchCurrency(value) {
      this.set('cryptocurrency', value);
    }
  }

});
