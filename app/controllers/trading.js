import Controller from '@ember/controller';
import { computed, observer } from '@ember/object';
import fetch from 'fetch';
import ENV from 'indaexchange/config/environment';

export default Controller.extend({
  ENV: ENV,

  currentlyLoading: computed('model.pair', function() {
    return Boolean(!this.get('model.pair'));
  }),

  //pair: 'ETH_BTC', // BTC_USD покупаю BTC за USD / base and quote
  pair: null,
  //selectedQuote: 'BTC',
  selectedQuote: null,
  currentOrdersData: [],
  currencyChartData: [],

  // Spinners
  pairSpinner: false,
  orderbookSpinner: false,
  historyTradeSpinner: false,
  historyUserSpinner: false,

  chartData: computed('model.offer', function() {
    const me = this;
    let data = {
      decreasing: [],
      increasing: [],
    };
    me.store.peekAll('offer').forEach(item => {
      if (item.get('type') === 'bid') data.decreasing.push(item);
      if (item.get('type') === 'ask') data.increasing.push(item);
    });
    return data;
  }),

  orderBookLastPrice: computed('model.offer', function() {
    const me = this;
    const record = this.store.peekRecord('offer', 1);
    if (record) {
      me.send('setPrice', record.get('price'));
      return record;
    } else {
      return null;
    }
  }),

  // TODO: Переписать на Task's, потому что сейчас при быстром перещелкивании загружаются старые данные. Либо разбить на несколько обсерверов
  refreshPairDate: observer('pair', function() {
    const me = this;
    const pair = me.get('pair');

    me.set('pairSpinner', true);
    me.set('orderbookSpinner', true);
    me.set('historyTradeSpinner', true);
    me.set('historyUserSpinner', true);

    // Store: pair
    me.store.peekAll('pair').forEach(item => {
      me.store.unloadRecord(item);
    });
    me.store.query('pair', { pairName: pair }).then(data => {
      me.set('model.pair', data);
      me.set('pairSpinner', false);
    });

    // Store: history-user
    me.store.peekAll('history-user').forEach(item => {
      me.store.unloadRecord(item);
    });
    // 'history-user': this.get('store').query('history-user', { operationFilter: 143 }),
    me.store.query('history-user', { operationFilter: 143 }).then(data => {
      me.set('model.history-user', data);
      me.set('historyUserSpinner', false);
    });

    // Store: current-order
    me.store.peekAll('current-order').forEach(item => {
      me.store.unloadRecord(item);
    });
    const orders = me.store.peekAll('current-order');
    me.set('currentOrdersData', orders);

    // Store: graph
    me.store.peekAll('graph').forEach(item => {
      me.store.unloadRecord(item);
    });
    const chartData = me.store.peekAll('graph');
    me.set('currencyChartData', chartData);

    // Store: offer
    me.store.peekAll('offer').forEach(item => {
      me.store.unloadRecord(item);
    });
    me.store.query('offer', { pairName: pair }).then(data => {
      me.set('model.offer', data);
      me.set('orderbookSpinner', false);
    });

    // Store: history-trade
    me.store.peekAll('history-trade').forEach(item => {
      me.store.unloadRecord(item);
    });
    me.store.query('history-trade', { pairName: pair, count: 30 }).then(data => {
      me.set('model.history-trade', data);
      me.set('historyTradeSpinner', false);
    });
  }),

  tickerInfo: computed('pair', function() {
    const pair = this.get('pair');
    return this.store.peekRecord('ticker', pair);
  }),

  currencyTableData: computed('selectedQuote', function() {
    const me = this;
    const selectedQuote = me.get('selectedQuote');
    let ticker = me.get('store').peekAll('ticker');
    let result = [];
    ticker.forEach(function(item) {
      if (item.get('quote') === selectedQuote) {
        //{{debugger}}
        const currency = me.store.peekRecord('currency', item.get('base'));
        let obj = {};
        obj.base = item.get('base');
        obj.quote = item.get('quote');
        if (currency) {
          obj.changeDay = currency.get('changeDay').toFixed(2);
          obj.changeWeek = currency.get('changeWeek').toFixed(2);
          obj.changeMonth = currency.get('changeMonth').toFixed(2);
          obj.price = currency.get('price.usd').toFixed(2);
        }
        // else {
        // 	obj.changeDay = '-';
        // 	obj.changeWeek = '-';
        // 	obj.changeMonth = '-';
        // 	obj.price = '-';
        // }
        result.push(obj);
      }
    });
    return result;
  }),

  // let obj = {
  // 	shortName: cryptocurrency,
  // 	amount: item.get('amount')
  // };
  // if (record) {
  // 	obj.name = record.get('name');
  // 	obj.price = record.get('price.usd').toFixed(2);
  // 	obj.usdEq = (record.get('price.usd') * item.get('amount')).toFixed(2);
  // 	obj.changeDay = record.get('changeDay').toFixed(0);
  // 	obj.changeWeek = record.get('changeWeek').toFixed(0);
  // 	obj.changeMonth = record.get('changeMonth').toFixed(0);
  // 	total += record.get('price.usd') * item.get('amount');
  // }
  // result.push(obj);

  // currencyTableData: computed('selectedQuote', function(){
  // 	const selectedQuote = this.get('selectedQuote');
  // 	let ticker = this.get('store').peekAll('ticker');
  // 	let res = ticker.filter(function(item) {
  // 		return item.get('quote') === selectedQuote;
  // 	});
  // 	return res;
  // }),

  tickerPairs: computed('model.ticker', function() {
    let ticker = this.get('store').peekAll('ticker');
    let data = {};
    let bases = [];
    let quotes = [];
    ticker.forEach(function(pair) {
      bases.push(pair.base);
      quotes.push(pair.quote);
    });
    data.bases = [...new Set(bases)];
    data.quotes = [...new Set(quotes)];
    return data;
  }),

  chartPair: computed('pair', function() {
    let pair = this.get('pair');
    return Ember.String.htmlSafe('<span>' + pair.split('_')[0] + '</span>/' + pair.split('_')[1]);
  }),

  // TODO: переписать на pair.pair = USD_BTC pair.base = USD pair.quote = BTC
  pairInfo: computed('pair', function() {
    let pair = this.get('pair');
    return pair;
  }),
  pairBase: computed('pair', function() {
    let pair = this.get('pair').split('_')[0];
    return pair;
  }),
  pairQuote: computed('pair', function() {
    let quote = this.get('pair').split('_')[1];
    return quote;
  }),

  availableBase: computed('pair', function() {
    const base = this.get('pair').split('_')[0];
    const balance = this.store.peekRecord('balance', base);
    if (balance) return balance.get('amount');
    else return 0;
  }),

  availableQuote: computed('pair', function() {
    const quote = this.get('pair').split('_')[1];
    const balance = this.store.peekRecord('balance', quote);
    if (balance) return balance.get('amount');
    else return 0;
  }),

  // Modal added
  isShowingDeleteQuestion: false,

  actions: {
    // Modal added
    toggleModal: function() {
      this.toggleProperty('isShowingWithoutOverlay');
    },

    setPrice(value) {
      $('#limit .operations__price').val(value);
      $('#stop-limit .operations__stop').val(value);
      $('#stop-limit .operations__limit').val(value);
      $('#stop-limit .operations__bid-price').val(value);
    },
    updatePair() {
      const me = this;
      me.notifyPropertyChange('pair');
    },

    // Modal added
    deleteOrderQuestion(orderId) {
      const me = this;
      me.set('isShowingDeleteQuestion', true);
    },

    deleteOrder(orderId) {
      const me = this;
      let data = JSON.stringify({
        OrderId: orderId,
      });
      let url = 'https://indacoin.com/inner/CancelOrder2';
      fetch(url, {
        method: 'POST',
        headers: {
          'content-type': 'application/json',
          accept: '*/*',
          'x-requested-with': 'XMLHttpRequest',
          origin: 'https://indacoin.com',
        },
        body: data,
      })
        .then(function(response) {
          if (response.ok) {
            return response.json();
          }
          throw new Error('Network response was not ok');
        })
        .then(function(response) {
          if (response.d === 'done') {
            $(`.current-orders tr.order-${orderId}`).remove();
            alert(`Success. Order #${orderId} deleted`);

            // TODO: divide Pair on many observables
            /*
					refresh:
					Current Orders
					left 2 tables + middle
					Available: 0.45649377 BTC
					*/

            me.notifyPropertyChange('pair');
          } else throw new Error(`Order #${orderId} not deleted`);
        })
        .catch(function(error) {
          alert(error);
        });
    },
  },
});
