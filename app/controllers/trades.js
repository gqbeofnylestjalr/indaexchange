import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  // TODO: While all models loadingm not one
  currentlyLoading: computed('model.pair', function() {
    return Boolean(!this.get('model.pair'));
  }),
});
