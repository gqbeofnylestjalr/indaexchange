import { helper } from '@ember/component/helper';

export function getBase(params /*, hash*/) {
  return params[0].split('_')[1];
}

export default helper(getBase);
