import { helper } from '@ember/component/helper';

export function getQuote(params /*, hash*/) {
  return params[0].split('_')[0];
}

export default helper(getQuote);
