import { helper } from '@ember/component/helper';

export function timestampToTime(params /*, hash*/) {
  return timeConverter(params[0]);
}

export default helper(timestampToTime);

function timeConverter(UNIX_timestamp) {
  let a = new Date(UNIX_timestamp * 1000);
  let hour = ('0' + a.getHours()).slice(-2);
  let min = ('0' + a.getMinutes()).slice(-2);
  let sec = ('0' + a.getSeconds()).slice(-2);
  return hour + ':' + min + ':' + sec;
}
