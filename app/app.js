import Application from '@ember/application';
import Resolver from './resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';
import { warn, deprecate, assert } from '@ember/debug';

const App = Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver
});

// TODO: нельзя игнорировать логи
Ember.warn = function() { };
//Ember.deprecate = function() { };
//Ember.assert = function() { };

loadInitializers(App, config.modulePrefix);

export default App;
