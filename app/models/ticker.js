import DS from 'ember-data';

export default DS.Model.extend({
  base: DS.attr('string'),
  quote: DS.attr('string'),
  low: DS.attr('number'),
  high: DS.attr('number'),
  firstid: DS.attr('number'),
  secondid: DS.attr('number'),

  //,offers: DS.hasMany('offer')
});

// [
// 	{"pair":"BTC_USD",
// 		"low":null,
// 		"high":null,
// 		"firstid":100,
// 		"secondid":2
// 	},
// 	{"pair":"BTC_EUR",
// 		"low":null,
// 		"high":null,
// 		"firstid":100,
// 		"secondid":7
// 	},
// 	{
// 		"pair":"ETH_USD"
// 		,"low":null,
// 		"high":null,
// 		"firstid":248,
// 		"secondid":2
// 	},{
// 		"pair":"ETH_EUR",
// 		"low":null,
// 		"high":null,
// 		"firstid":248,
// 		"secondid":7
// 	},{
// 		"pair":"BAT_ETH",
// 		"low":null,
// 		"high":null,
// 		"firstid":358,
// 		"secondid":248
// 	},{
// 		"pair":"DARK_KOBO",
// 		"low":4.0000000000,
// 		"high":6.5000000000,
// 		"firstid":600,
// 		"secondid":733
// 	}
// ]
