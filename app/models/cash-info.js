import DS from 'ember-data';

export default DS.Model.extend({
  typeId: DS.attr('number'),
  factCurrencyId: DS.attr('string'),
  minSum: DS.attr('number'), // 30$
  maxSum: DS.attr('number'), // 3000$
  feeFactPercent: DS.attr('number'), // 9%
  minFee: DS.attr('number'), // 2.7$

  /*

The fee is 9.0% but not less than 5$ . The minimum amount is 20$, maximum - is 500$.

      "{"cashTypes":{
      "cashIn":{
        "50":{
          "typeId":50,
          "currencyId":"EUR",
          "currencyIdInt":7,
          "factCurrencyId":"EUR",
          "fields":{"name":"QuickPay","fields":[{"name":"amount","params":{"currency":"USD"}},{"name":"commonFee"},{"name":"phone"},{"name":"fio","type":"input","params":{"regexp":".+"}},{"name":"bday","type":"input","params":{"regexp":"^[0-9]{2}_[0-9]{2}_[0-9]{4}$","mask":"**_**_****","mask_placeholder":"MM_DD_YYYY"}}]},
          "fee":{
            "minSum":30.00000000,
            "maxSum":3000.00000000,
            "reserv":0.0,
            "minFee":2.700000000,
            "feeAdd":0.00000000,
            "feePercent":0.09000,
            "feeFactPercent":0.09000,
            "feeFactAdd":0.25000000,
            "feeFactExtraPercent":0.00000000,
            "feeFactExtraAdd":0.00000000,
            "defaultValue":30.00000000},
            "fullName":"Credit Cards QuickPay",
            "isCrypto":0,
            "execution":"0",
            "shortcode":"CARDEUR",
            "commentPath":"CashTypes.Deposit.MasterCardProblem",
            "limitDay":null,
            "limitWeek":null,
            "limitMonth":null},
        "54":{
          "typeId":54,
          "currencyId":"USD",
          "currencyIdInt":4,
          "factCurrencyId":"RUB","fie*/

  // minFee: DS.attr('number'),
  // feePercent: DS.attr('number'),

  // usdAvailable: DS.attr('boolean'),
  // eurAvailable: DS.attr('boolean'),
  // rubAvailable: DS.attr('boolean'),

  // btcAvailable: DS.attr('boolean'),
  // audAvailable: DS.attr('boolean'),
  // gbpAvailable: DS.attr('boolean'),

  // usdmin: DS.attr('number'),
  // usdmax: DS.attr('number'),
  // eurmin: DS.attr('number'),
  // eurmax: DS.attr('number'),
  // rubmin: DS.attr('number'),
  // rubmax: DS.attr('number'),

  // btcmin: DS.attr('number'),
  // btcmax: DS.attr('number'),
  // audmin: DS.attr('number'),
  // audmax: DS.attr('number'),
  // gbpmin: DS.attr('number'),
  // gbpmax: DS.attr('number'),
  // test: DS.attr('string'),

  // usdCommentPath: DS.attr('string'),
  // eurCommentPath: DS.attr('string'),
  // rubCommentPath: DS.attr('string'),
  // btcCommentPath: DS.attr('string'),
  // audCommentPath: DS.attr('string'),
  // gbpCommentPath: DS.attr('string'),

  // changeDirection: DS.attr('boolean')
});
