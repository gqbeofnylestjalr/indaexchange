import DS from 'ember-data';

export default DS.Model.extend({
  cur_id: DS.attr('number'),
  ext_market: DS.attr('number'),
  txfee: DS.attr('string'),
  short_name: DS.attr('string'),
  name: DS.attr('string'),
  cointype: DS.attr('string'),
  imageurl: DS.attr('string'),
  availableSupply: DS.attr('string'),
  price: DS.attr('string'),
  tag: DS.attr('boolean'),
  isActive: DS.attr('boolean'),
  withdrawEnabled: DS.attr('boolean'),
  changeDay: DS.attr('string'),
  changeWeek: DS.attr('string'),
  changeMonth: DS.attr('string'),
});
