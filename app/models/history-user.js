import DS from 'ember-data';

export default DS.Model.extend({
  txid: DS.attr('string'),
  date: DS.attr('string'),
  operation_type: DS.attr('string'),
  status: DS.attr('string'),
  is_crypto: DS.attr('boolean'),
  add_info: DS.attr('string'),
  price: DS.attr('number'),
  amount_cur: DS.attr('number'),
  total_cur: DS.attr('string'),
  total: DS.attr('number'),
  fee_cur: DS.attr('string'),
  fee: DS.attr('number'),
  amount: DS.attr('number'),
  typeId: DS.attr('number'),
});

// {"d":"[{\"id\":\"D2627235\",\"date\":\"09/28/2018 17:49:13 +00:00\",\"type\":\"Deposit\",\"addInfo\":{\"typeId\":50,\"status\":\"not_paid\",
// \"is_crypto\":0,\"add_info\":null},\"price\":null,\"amount_cur\":null,\"amount\":null,\"total_cur\":\"EUR\",\"total\":100.0000000000000,
// \"fee_cur\":\"EUR\",\"fee\":0.0}]"}
