import DS from 'ember-data';

export default DS.Model.extend({
  userName: DS.attr('string'),

  orders: DS.hasMany('current-order', { async: true }),
  graph: DS.hasMany('graph', { async: true }),
  balance: DS.hasMany('balance', { async: true }),
});
