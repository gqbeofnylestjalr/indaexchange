import DS from 'ember-data';

export default DS.Model.extend({
  orderId: DS.attr('number'),
  pair: DS.attr('string'), // "pair":"ETH_USD",
  time: DS.attr('number'), // "DT":1538746816
  side: DS.attr('boolean'), // "B":true,
  orderPrice: DS.attr('number'), // "P":34.0000000000,
  orderAmount: DS.attr('number'), // "A":408.00000000,
  unexecuted: DS.attr('number'), // ??? "C":12.00000000,
  executed: DS.attr('number'), // ??? "F":-4.08000000,

  relatedPair: DS.belongsTo('pair', { async: true }),
});

// "Id":151,
// "CI":248,
// +"pair":"ETH_USD",
// +"C":12.00000000,
// +"P":34.0000000000,
// +"A":408.00000000,
// +"F":-4.08000000,
// +"B":true,
// +"DT":1538746816
