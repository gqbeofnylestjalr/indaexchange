import DS from 'ember-data';

export default DS.Model.extend({
  phoneId: DS.attr('number'),
  phoneAuthorizationCode: DS.attr('string'),
  phoneNumber: DS.attr('string'),
  phoneStatusAuthorizationCode: DS.attr('string'),
  attemptsAuthorizationCode: DS.attr('number'),
});
