import DS from 'ember-data';

export default DS.Model.extend({
  max: DS.attr('number'),
  min: DS.attr('number'),
  open: DS.attr('number'),
  close: DS.attr('number'),
  volume: DS.attr('number'),
  timestamp: DS.attr('number'),

  relatedPair: DS.belongsTo('pair', { async: true }),
});
