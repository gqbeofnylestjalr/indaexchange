import DS from 'ember-data';

export default DS.Model.extend({
  pair: DS.attr('string'),
  type: DS.attr('string'),
  price: DS.attr('string'),
  base_amount: DS.attr('number'),
  quote_amount: DS.attr('number'),
  fee_amount: DS.attr('number'),

  //primaryKey: 'username' And use your username as an id.
});

/*{
    "bid":[
        {
            "price":3.5000000000,
            "base_amount":2000.0,
            "quote_amount":7000.0,
            "fee_amount":7.0
        },
        {
            "price":3.0000000000,
            "base_amount":1000.0,
            "quote_amount":3000.0,
            "fee_amount":6.0
        }
    ],
    "ask":[
        {
            "price":6.5000000000,
            "base_amount":1000.0,
            "quote_amount":6500.0,
            "fee_amount":6.5
        },
        {
            "price":7.0000000000,
            "base_amount":1000.0,
            "quote_amount":7000.0,
            "fee_amount":7.0
        },
        {
            "price":7.5000000000,
            "base_amount":1000.0,
            "quote_amount":7500.0,
            "fee_amount":7.5
        }
    ]
}*/
