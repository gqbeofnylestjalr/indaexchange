import DS from 'ember-data';

export default DS.Model.extend({
  card_id: DS.attr('number'),
  created_at: DS.attr('string'),
  card_type: DS.attr('string'),
  card_number: DS.attr('number'),
  authorization_code: DS.attr('string'),
  limit_remain: DS.attr('string'),
  status_random_sum_string: DS.attr('string'),
  attempts_random_sum: DS.attr('number'),
  status_authorization_code_string: DS.attr('string'),
  attempts_auth_code: DS.attr('number'),
  status_string: DS.attr('string'),
});
