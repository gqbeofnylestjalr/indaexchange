import DS from 'ember-data';

export default DS.Model.extend({
  currency: DS.attr('string'),
  amount: DS.attr('number'),

  relatedPair: DS.belongsTo('pair', { async: true }),
});

// TODO: default values and methods in model https://github.com/emjs/guides/blob/master/source/testing/testing-models.md