import DS from 'ember-data';

export default DS.Model.extend({
  wallet: DS.attr('string'),
  hash: DS.attr('string'),
});
