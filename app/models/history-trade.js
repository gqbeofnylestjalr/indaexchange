import DS from 'ember-data';

export default DS.Model.extend({
  base: DS.attr('number'),
  quote: DS.attr('number'),
  price: DS.attr('number'),
  timestamp: DS.attr('number'),
});
