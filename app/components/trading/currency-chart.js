import Component from '@ember/component';
import Plotly from 'plotly';

export default Component.extend({
  didInsertElement: function(){
    this._super(...arguments);

    let currencyChartData = this.get('currencyChartData');
    let chartData = {};
    chartData.x = [];
    chartData.close = [];
    chartData.decreasing = {line: {color: '#e15ec8'}};
    chartData.high = [];
    chartData.increasing = {line: {color: '#12c8db'}};
    chartData.line = {color: 'rgba(31,119,180,1)'};
    chartData.low = [];
    chartData.open = [];
    chartData.type = 'candlestick';
    chartData.xaxis = 'x';
    chartData.yaxis = 'y';

    let startDate = currencyChartData.firstObject.timestamp;
    const endDate = currencyChartData.lastObject.timestamp;
    const startRange = 5 + 5/24; // 5 days and 5 hours
    const minimumPoints = 40;
    let xAxisAutorange = true;
    if (differenceDays(startDate, endDate) > startRange) {
      startDate = endDate - startRange * 1000 * 60 * 60 * 24;
      if (currencyChartData.length > minimumPoints)
        xAxisAutorange = false;
    }

    currencyChartData.forEach(function(item){
      chartData.x.push(item.timestamp);
      chartData.open.push(item.open);
      chartData.high.push(item.max);
      chartData.low.push(item.min);
      chartData.close.push(item.close);
    });

    var data = [chartData];

    var layout = {
      dragmode: 'zoom',
      margin: {
        r: 10, 
        t: 25, 
        b: 40, 
        l: 60
      },
      showlegend: false,
      xaxis: {
        autorange: xAxisAutorange,
        domain: [0, 1],
        range: [startDate, endDate],
        title: 'Date',
        type: 'date'
      }, 
      yaxis: {
        autorange: true, 
        domain: [0, 1],
        type: 'linear'
      }
    };
    
    Plotly.plot('plotly-div', data, layout);
  }
});

function differenceDays(firstTimestamp, secondTimestamp) {
  return Math.abs(firstTimestamp - secondTimestamp)/1000/60/60/24;
}