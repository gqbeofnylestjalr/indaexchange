import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  operationType: 'buy',

  limitErrorMessage: '',
  marketErrorMessage: '',
  stopLimitErrorMessage: '',

  getOperationType: computed('operationType', function() {
    if (this.get('operationType') === 'buy')
      return "Buy";
    else
      return "Sell";
  }),

  didInsertElement: function() {
    const me = this;
    me._super(...arguments);

/****************************************/
/********* Change inputs Limit **********/
/****************************************/
    const limitPrice = me.$("#limit .operations__price"); // 10 USD
    const limitAmount = me.$("#limit .operations__amount"); // 3 BTC
    const limitTotal = me.$("#limit .operations__total"); // 30 USD

    // Not work with type=number
    // limitPrice.on("keypress", function(e) {
    //   let value = limitPrice.val();
    //   console.log('value ' + value);
    //   {{debugger}}
    //   e = e || event;
    //   if (e.ctrlKey || e.altKey || e.metaKey) return;
    //   var chr = getChar(e);
    //   if ((chr === '.' || chr === ',') && (value.indexOf('.') !== -1 || value.indexOf(',') !== -1))
    //     return false;
    //   if (chr == null) return;
    //   if (chr === '.' || chr === ',') {}
    //   else if (chr < '0' || chr > '9') {
    //     return false;
    //   }
    // });

    limitPrice.on("keyup", function() {
      if (limitPrice.val() === '' || limitAmount.val() === '') {
        limitTotal.val('');
      } else {
        let result = limitPrice.val() * limitAmount.val();
        limitTotal.val(result.toFixed(8));
      }
    });
    limitPrice.on("focus", function() {
      me.set("limitErrorMessage", "");
      me.$("#limit .operations__price").removeClass("error");
    });

    limitAmount.on("keyup", function() {
      me.$("#limit .operations__amount").removeClass("error");
      if (limitPrice.val() === '' || limitAmount.val() === '') {
        limitTotal.val('');
      } else {
        let result = limitPrice.val() * limitAmount.val();
        limitTotal.val(result.toFixed(8));
      }
    });
    limitAmount.on("focus", function() {
      me.set("limitErrorMessage", "");
    });

    limitTotal.on("keyup", function() {
      me.set("limitErrorMessage", "");
      if (limitPrice.val() === '') {
        limitAmount.val('');
      } else {
        let result = limitTotal.val() / limitPrice.val();
        limitAmount.val(result.toFixed(6));
      }
    });

/****************************************/
/********* Change inputs Market *********/
/****************************************/
const marketAmount = me.$("#market .operations__amount"); // 3 BTC

marketAmount.on("focus", function() {
  me.set("marketErrorMessage", "");
  me.$("#market .operations__amount").removeClass("error");
})

// marketAmount.on("keyup", function() {
//   me.set("marketErrorMessage", "");
//   me.$("#market .operations__amount").removeClass("error");
// });


/****************************************/
/********* Change inputs Stop-limit **********/
/****************************************/
    //const stopStop = $("#stop-limit .operations__stop"); // 10 USD
    const stopLimit = me.$("#stop-limit .operations__limit"); // 3 BTC
    const stopAmount = me.$("#stop-limit .operations__amount"); // 30 USD
    const stopTotal = me.$("#stop-limit .operations__total"); // 30 USD

    stopLimit.on("keyup", function() {
      if (stopLimit.val() === '' || stopAmount.val() === '') {
        stopTotal.val('');
      } else {
        let result = stopLimit.val() * stopAmount.val();
        stopTotal.val(result.toFixed(8));
      }
    });

    stopAmount.on("keyup", function() {
      if (stopLimit.val() === '' || stopAmount.val() === '') {
        stopTotal.val('');
      } else {
        let result = stopLimit.val() * stopAmount.val();
        stopTotal.val(result.toFixed(8));
      }
    });

    stopTotal.on("keyup", function() {
      if (stopLimit.val() === '') {
        stopAmount.val('');
      } else {
        let result = stopTotal.val() / stopLimit.val();
        stopAmount.val(result.toFixed(6));
      }
    });

  },
  actions: {

    setOperationType(type) {
      this.set('operationType', type);
    },

    addNewOrder() {

      // TODO: jquery get form data
      //var formData = $(".operations__form form").form('get values');
      //inner/AddNewOrder2(string pair, OperationType ot, decimal amount, decimal price, bool fixBase)

      const me = this;
      const price = Number(this.$("#limit .operations__price").val());
      const amount = Number(this.$("#limit .operations__amount").val());
      const total = Number(this.$("#limit .operations__total").val());

      const ot = this.get('operationType');
      const fixBase = true;

      const pair = this.get('pair');

      let balance = null;
      if (ot === 'buy')
        balance = this.get('availableQuote');
      else
        balance = this.get('availableBase');

      //const form = $("#limit form");
      //var form = document.getElementById('limit-form');

      /* *********** */
      /* Validations */
      /* *********** */


      // if (form.checkValidity() === true) {
      //   event.preventDefault()
      //   event.stopPropagation()
      // }


      if (!price) {
        this.set("limitErrorMessage", "Please input correct price");
        this.$("#limit .operations__price").addClass("error");
        return;
      }

      if (!amount) {
        this.set("limitErrorMessage", "Please input correct amount");
        this.$("#limit .operations__amount").addClass("error");
        return;
      }

      if (total > balance) {
        this.set("limitErrorMessage", "Your balance is not enough");
        this.$("#limit .operations__amount").addClass("error");
        return;
      }

      /* *************** */
      /* End validations */
      /* *************** */

      const data = JSON.stringify({
        pair: pair,
        ot: ot,
        amount: amount,
        price: price,
        fixBase: fixBase
      });
      const url = "https://indacoin.com/inner/AddNewOrder2";

      fetch(url, {
        method: 'POST',
        headers: {
          "content-type": "application/json",
          "accept": "*/*",
          "x-requested-with": "XMLHttpRequest",
          "origin": "https://indacoin.com"
        },
        body: data
      }).then(function(response) {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok\r\n' + response.d.error_string);
      }).then(function(response) {

        if (response.d.id) {
          alert('Order created, id: ' + response.d.id);
          me.updatePair();
        }
        else {
          if (response.d.error_string === 'No orders')
            alert('No orders');
          else if (response.d.error_string === 'Insufficient funds')
            alert('Insufficient funds');
          else if (response.d.error_string === 'User not authorized')
            alert('User not authorized');
          else
            alert('Unknown error');
        }
        
      }).catch(function(error) {
        alert(error);
      });

    },

    //public static Market_Core_ANSWER MarketOperation2(string pair, OperationType ot, decimal amount, bool fixBase)
    marketOperation() {
      const me = this;
      const amount = this.$("#market .operations__amount").val();
      const ot = this.get('operationType');
      const fixBase = true;
      const pair = this.get('pair');

      let balance = null;
      if (ot === 'buy')
        balance = this.get('availableQuote');
      else
        balance = this.get('availableBase');

      /* *********** */
      /* Validations */
      /* *********** */

      if (!amount) {
        if (ot === 'buy')
          me.set("marketErrorMessage", "Please input buy amount");
        else
          me.set("marketErrorMessage", "Please input sell amount");
        me.$("#market .operations__amount").addClass("error");
        return;
      }

      if (amount > balance) {
        this.set("marketErrorMessage", "Your balance is not enough");
        this.$("#market .operations__amount").addClass("error");
        return;
      }

      /* *************** */
      /* End validations */
      /* *************** */

      const data = JSON.stringify({
        pair: pair,
        ot: ot,
        amount: amount,
        fixBase: fixBase
      });
      const url = "https://indacoin.com/inner/MarketOperation2";
      fetch(url, {
        method: 'POST',
        headers: {
          "content-type": "application/json",
          "accept": "*/*",
          "x-requested-with": "XMLHttpRequest",
          "origin": "https://indacoin.com"
        },
        body: data
      }).then(function(response) {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok\r\n' + response.d.error_string);
      }).then(function(response) {
        if (response.d.id) {
          alert('Order created, id: ' + response.d.id);
          me.updatePair();
        }
        else {
          if (response.d.error_string === 'No orders')
            alert('No orders');
          else if (response.d.error_string === 'Insufficient funds')
            alert('Insufficient funds');
          else if (response.d.error_string === 'User not authorized')
            alert('User not authorized');
          else
            alert('Unknown error');
        }
      }).catch(function(error) {
        alert(error);
      });
    },

    //AddNewOrderStopLoss2(string pair, OperationType ot, decimal amount, decimal price, bool fixBase, decimal stopLossActivation, decimal stopLossBidPrice)
    addNewOrderStopLoss() {
      const me = this;
      const stop = me.$("#stop-limit .operations__stop").val();
      const limit = me.$("#stop-limit .operations__limit").val();
      const amount = me.$("#stop-limit .operations__amount").val();
      const bidPrice = me.$("#stop-limit .operations__bid-price").val();
      const ot = me.get('operationType');

      let balance = null;
      if (ot === 'buy')
        balance = me.get('availableQuote');
      else
        balance = me.get('availableBase');

      /* *********** */
      /* Validations */
      /* *********** */

      if (!Number(amount)) {
        if (ot === 'buy')
          me.set("stopLimitErrorMessage", "Please input buy amount");
        else
          me.set("stopLimitErrorMessage", "Please input sell amount");
        me.$("#stop-limit .operations__amount").addClass("error");
        return;
      }

      if (amount > balance) {
        me.set("stopLimitErrorMessage", "Your balance is not enough");
        me.$("#stop-limit .operations__amount").addClass("error");
        return;
      }

      /* *************** */
      /* End validations */
      /* *************** */

      const pair = me.get('pair');
      const fixBase = true;
      const data = JSON.stringify({
        pair: pair,
        ot: ot,
        amount: amount,
        price: limit,
        fixBase: fixBase,
        stopLossActivation: stop,
        stopLossBidPrice: bidPrice
      });
      const url = "https://indacoin.com/inner/AddNewOrderStopLoss2";
      fetch(url, {
        method: 'POST',
        headers: {
          "content-type": "application/json",
          "accept": "*/*",
          "x-requested-with": "XMLHttpRequest",
          "origin": "https://indacoin.com"
        },
        body: data
      }).then(function(response) {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok\r\n' + response.d.error_string);
      }).then(function(response) {
        if (response.d.id) {
          alert('Order created, id: ' + response.d.id);
          me.updatePair();
        }
        else {
          if (response.d.error_string === 'No orders')
            alert('No orders');
          else if (response.d.error_string === 'Insufficient funds')
            alert('Insufficient funds');
          else if (response.d.error_string === 'User not authorized')
            alert('User not authorized');
          else
            alert('Unknown error');
        }
      }).catch(function(error) {
        alert(error);
      });

    },

  }
});

function getChar(event) {
  if (event.which == null) {
    if (event.keyCode < 32) return null;
    return String.fromCharCode(event.keyCode) // IE
  }
  if (event.which != 0 && event.charCode != 0) {
    if (event.which < 32) return null;
    return String.fromCharCode(event.which) // Others
  }

  return null; // Special key
}