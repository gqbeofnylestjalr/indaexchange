import Component from '@ember/component';

export default Component.extend({
  didInsertElement: function() {
    this._super(...arguments);
    // TODO: заменить $ на this.$ - но это ломает сборку. Возможно, js для табов поздно подключается
    // TODO: поменять на получение из ticker первого элемента

    // TODO: if none quote
    const quote = this.get("pairQuote");
    $(`#currency-statistics__tabs a[href="#${quote}"]`).tab('show');
  },
  actions: {
    setPair(base, quote) {
      this.set('pair', `${base}_${quote}`);

      // TODO: переписать на class="{{#if }}active{{/if}}""
      this.$('.currency-statistics tbody tr').removeClass('active');
      this.$(`.currency-statistics .currency-table__row-${base}`).addClass('active');
    },
    setQuote(quote){
      this.set('selectedQuote', quote);
    }
  }
});
