import Component from '@ember/component';

export default Component.extend({
  didInsertElement: function() {
    this._super(...arguments);

    const mask = this.get('phoneVerification').get('phoneAuthorizationCode').replace(/\*/g, '0');
    $('#phone-verification-form .phone-verification-form__code').mask(mask, {
      onComplete: function(){
        //$('.payment-methods__card input[name=payment-methods__phone]').focus();
      }
    });

  },

  actions: {

    //url: "/cards/CheckPhoneAuthCode", data: $.toJSON({ phoneId: tablePhones.data("phone_id"), authCode: authCode }),
    checkPhoneAuthCode(){
        const me = this;
        //const phoneId = me.store.peekRecord('phone').get('card_id');
        const authCode = $('#phone-verification-form .phone-verification-form__code').val();
        const phoneId = me.get('phoneVerification').get('phoneId');
        if (!authCode || !phoneId) {
          alert('Missing auth-code or phoneID');
          return;
        }
				const data = JSON.stringify({
          phoneId: phoneId,
          authCode: authCode
				});
				const url = "https://indacoin.com/cards/CheckPhoneAuthCode";
				fetch(url, {
					method: 'POST',
					headers: {
						"content-type": "application/json",
						"accept": "*/*",
						"x-requested-with": "XMLHttpRequest",
						"origin": "https://indacoin.com"
					},
					body: data
				}).then(function(response) {
					if(response.ok) {
						return response.json();
					}
					throw new Error('Network response was not ok');
				}).then(function(response) {
					if (response.d) {
            alert(`Success`);
            me.set('phoneVerification', false);
					}
					else
						throw new Error(`Failure`);
				}).catch(function(error) {
					alert(error);
        });
    }

  }
});
