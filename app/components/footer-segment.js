import Component from '@ember/component';
import ENV from 'indaexchange/config/environment';

export default Component.extend({
  ENV: ENV,
});
