import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  i18n: service(),
  classNames: ['language-select'],

  locales: computed('i18n.{locale,locales}', function() {
    const i18n = this.get('i18n');
    return this.get('i18n.locales').map(function(loc) {
      return { id: loc, text: i18n.t('language-select.language.' + loc) };
    });
  }),

  actions: {
    setLocale() {
      this.set('i18n.locale', this.$('select').val());
    },
  },
});
