import Component from '@ember/component';
import Chart from 'Chart';

export default Component.extend({

  didInsertElement: function(){
    const me = this;
    me._super(...arguments);

    var ctx = document.getElementById("balances-chart").getContext('2d');

//     $crypto-color-one: #12c8db
// $crypto-color-two: #bbd5ff
// $crypto-color-three: #404eaa
// $crypto-color-four: "#012a77"

    let backgroundColor = [
      "#00cdfd",
      "#404eaa",
      "#bbd5ff",
      "#012a77",
      "#bbd5ff",
      "#012a77",
      "#0093ff",
      "#00ffff",
      "#60d2f5"
    ];

    let datasets = [{
      data: [],
      backgroundColor: []
    }];
    let labels = [];

    me.get('balance').forEach(function(item){
      let price = 0;
      me.get('currency').forEach(function(record){
        if (item.currency === record.short_name)
        price = record.price.usd;
      });
      datasets[0].data.push(
        (item.amount * price).toFixed(2)
      );
      datasets[0].backgroundColor.push(backgroundColor.shift());
      labels.push(item.currency);
    });
    // console.log('labels'); console.log(labels);
    // console.log('datasets'); console.log(datasets);

    // TODO: remake to get api https://indacoin.com/api/uni/mobgetcurrenciesinfoi/1
    // let prices = [
    //   "6317.77",
    //   "0.048",
    //   "0.01059140"
    // ];


    var data = {
      datasets: datasets,
      // datasets: [{
      //     data: [10, 20, 30],
      //     backgroundColor: [
      //       "#12c8db",
      //       "#bbd5ff",
      //       "#404eaa"
      //   ],
      // }],
  
      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: labels
      // labels: [
      //     'BTC',
      //     'DARK',
      //     'SEX'
      // ]
    };
    var options = {
      legend: {
        display: false
      },
      tooltips: {
        callbacks: {
          label: function(tooltipItem, data) {
            return data.labels[tooltipItem.index] + ': ' + 
                   data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + '$';
          }
          // 433: 20213.43$
        },
        enabled: true,
        //tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' %' %>",
        //tooltipTemplate: "<%= value + ' %' %>",
        //afterBody: [' $']
        // afterTitle: {
        //   callback: function(tooltipItem, data) {
        //     console.log(tooltipItem);
        //     console.log(data);
        //     return tooltipItem + ' $';
        //   }
        // }
        // afterTitle: function(tooltipItem, data) {
        //   console.log(tooltipItem);
        //   console.log(data);
        // }
      }
    };

    var myDoughnutChart = new Chart(ctx, {
      type: 'doughnut',
      data: data,
      options: options
    });


  }

});
