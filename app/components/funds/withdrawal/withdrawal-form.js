import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  i18n: service(),
  store: service(),
  cryptocurrency: 'BTC',
  errorMessage: '',

  cryptocurrencyList: computed('balance', function() {
    let result = [];
    this.get('balance').forEach(function(item) {
      result.push(item.get('currency'));
    });
    return result;
  }),

  availableMessage: computed('cryptocurrency', function() {
    const i18n = this.get('i18n');
    const cryptocurrency = this.get('cryptocurrency');
    const record = this.get('store').peekRecord('balance', cryptocurrency);
    const available = record.get('amount') + ' ' + cryptocurrency; // 30 BTC
    return Ember.String.htmlSafe(i18n.t('funds.withdrawal.available-message', { available: available }));
  }),

  feesMessage: computed('cryptocurrency', function() {
    const i18n = this.get('i18n');
    const cryptocurrency = this.get('cryptocurrency');
    const record = this.get('store').peekRecord('currency', cryptocurrency);
    const fees = record.get('txfee') + ' ' + cryptocurrency; // 0.003 BTC
    return Ember.String.htmlSafe(i18n.t('funds.withdrawal.fees-message', { fees: fees }));
  }),

  isTag: computed('cryptocurrency', function() {
    const cryptocurrency = this.get('cryptocurrency');
    const record = this.get('store').peekRecord('currency', cryptocurrency);
    return record.get('tag');
  }),

  didInsertElement: function() {
    const me = this;
    const walletField = me.$(".withdrawal .withdrawal__address");
    const amountField = me.$(".withdrawal .withdrawal__amount");

    walletField.on("focus", function() {
      me.set("errorMessage", "");
      me.$(".withdrawal .withdrawal__address").removeClass("error");
      me.$(".withdrawal .withdrawal__amount").removeClass("error");
    });

    amountField.on("focus", function() {
      me.set("errorMessage", "");
      me.$(".withdrawal .withdrawal__address").removeClass("error");
      me.$(".withdrawal .withdrawal__amount").removeClass("error");
    });

  },

  actions: {
    toggleModal: function() {
      this.toggleProperty('isShowingModal');
    },
    // setCurrentWallet() {
    //   const me = this;
    //   const walletField = me.$(".withdrawal .withdrawal__address");
    //   const cryptocurrencyWallet = me.store.peekRecord('crypto-address', me.get('cryptocurrency'));
    //   walletField.val(cryptocurrencyWallet);
    // },

    switchCurrency(value) {
      this.set('cryptocurrency', value);
    },

    // {'amount':'12', 'jsonCashoutInfo':'{"name":"DARK","btcAddress":"21212","tag":"121"}', 'outputType':'44', 'TOTP':'-1', 'infoId':-1, 'info':''}
    registerPaymentOut() {
      const me = this;
      const i18n = me.get('i18n');
      const formData = $('.withdrawal .withdrawal__form form').serializeArray();

      const cryptocurrency = me.get('cryptocurrency');
      const wallet = formData[0].value;
      const amount = formData[1].value;

      let tag = '';
      if (formData[2])
        tag = formData[2].value;
      const outputType = '44';
      const TOTP = '-1';
      const infoId = '-1';
      const info = '';

      /* ************ */
      /* Check wallet */
      /* ************ */
      // var data = JSON.stringify({ currency: cryptocurrency, address: wallet });
      // const url = "https://indacoin.com/api/check_addr";
      // fetch(url, {
      //   method: 'POST',
      //   headers: {
      //     "content-type": "application/json",
      //     "accept": "*/*",
      //     "x-requested-with": "XMLHttpRequest",
      //     "origin": "https://indacoin.com"
      //   },
      //   body: data
      // }).then(function(response) {
      //   if(response.ok) {
      //     return response.json();
      //   }
      //   throw new Error('Network response was not ok');
      // }).then(function(response) {
      //   if (response.result === "not_valid") {
      //     me.set("errorMessage", i18n.t('funds.withdrawal.correct-crypto-message'));
      //     return;
      //   }
      //   else
      //     alert('call next');
      // }).catch(function(error) {
      //   alert(error);
      // });
      // return;

      /* ************ */
      /* End check wallet */
      /* ************ */



      /* *********** */
      /* Validations */
      /* *********** */
      if (!wallet) {
        this.set("errorMessage", "Please input correct cryptocurrency address");
        this.$(".withdrawal .withdrawal__address").addClass("error");
        return;
      }

      if (!Number(amount)) {
        this.set("errorMessage", "Please input correct amount address");
        this.$(".withdrawal .withdrawal__amount").addClass("error");
        return;
      }

      // if (!amount) {
      //   this.set("limitErrorMessage", "Please input correct amount");
      //   this.$("#limit .operations__amount").addClass("error");
      //   return;
      // }

      // if (total > balance) {
      //   this.set("limitErrorMessage", "Your balance is not enough");
      //   this.$("#limit .operations__amount").addClass("error");
      //   return;
      // }
      /* *************** */
      /* End validations */
      /* *************** */


      // {'amount':'12', 'jsonCashoutInfo':'{"name":"BTC","btcAddress":"221112","tag":"2221"}', 'outputType':'44', 'TOTP':'-1', 'infoId':-1, 'info':''}
      // {"amount":"0.001","jsonCashoutInfo":{"name":"BTC","btcAddress":"fffff","tag":"ddd"},"outputType":"44","TOTP":"-1","infoId":"-1","info":""}

      // const data = JSON.stringify({
      //   amount: amount,
      //   jsonCashoutInfo: {
      //     name: cryptocurrency,
      //     btcAddress: cryptocurrencyWallet,
      //     tag: tag
      //   },
      //   outputType: outputType,
      //   TOTP: TOTP,
      //   infoId: infoId,
      //   info: info
      // });

      const data = `{'amount':'${amount}', 'jsonCashoutInfo':'{"name":"${cryptocurrency}","btcAddress":"${wallet}","tag":"${tag}"}', 'outputType':'44', 'TOTP':'-1', 'infoId':-1, 'info':''}`;

      const url = "https://indacoin.com/funding/RegisterPaymentOut";
      fetch(url, {
        method: 'POST',
        headers: {
          "content-type": "application/json",
          "accept": "*/*",
          "x-requested-with": "XMLHttpRequest",
          "origin": "https://indacoin.com"
        },
        body: data
      }).then(function(response) {
        if(response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok');
      }).then(function(response) {
        if (response.d.Item1 === 1) {
          alert('Confirmation has been sent to your email address');
        }
        else
          throw new Error(`Failure: ${response.d.Item2}`);
      }).catch(function(error) {
        alert(error);
      });

    }
  }
});
