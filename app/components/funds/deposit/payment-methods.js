import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import ENV from 'indaexchange/config/environment';

export default Component.extend({
  ENV: ENV,
  store: service(),
  i18n: service(),

  method: 'card', // [card, bank, cryptocurrency]
  currency: 'USD', // [USD, EUR, RUB, AUD, GBP]
  amount: '', // 105.23
  //fee: '0.09',
  registerPaymentDisabled: false,
  paymentMethodsErrorMessage: '',

  copiedToClipboard : false,

  currencySymbol: computed("currency", function() {
    const currency = this.get("currency");
    switch(currency) {
      case 'USD':
        return '$';
      case 'EUR':
        return '€';
      case 'RUB':
        return '₽';
      case 'AUD':
        return 'AU$';
      case 'GBP':
        return '£';
      default:
      return '$';
    }
  }),

  fee: computed("amount", "currencySymbol", function() {
    const currency = this.get("currency");
    const record = this.get('store').peekRecord('cash-info', currency);
    return record.get('feeFactPercent');
  }),

  feeMessage: computed("currencySymbol", function() {
    const currency = this.get("currency"); // 'USD'
    const symbol = this.get("currencySymbol");
    const record = this.get('store').peekRecord('cash-info', currency);

    const minSum = record.get('minSum') + symbol; // 30$
    const maxSum = record.get('maxSum') + symbol; // 3000$

    let feeFactPercent = record.get('feeFactPercent') * 100; // 9%
    if (precision(feeFactPercent) > 2)
    feeFactPercent = feeFactPercent.toFixed(2);

    let minFee = record.get('minFee'); // 2.7$
    if (precision(minFee) > 2)
    minFee = minFee.toFixed(2);

    const i18n = this.get('i18n');
    return Ember.String.htmlSafe(i18n.t('funds.deposit.fee-message', { minSum: minSum, maxSum: maxSum, feeFactPercent: feeFactPercent, minFee: minFee + symbol }));
  }),

  commision: computed("amount", "currencySymbol", function() {
    const fee = parseFloat(this.get("fee"));
    const amount = 0 | parseFloat(this.get("amount"));
    const symbol = this.get("currencySymbol");
    return symbol + (fee * amount).toFixed(2);
  }),

  totalAmount: computed("amount", "currencySymbol", function() {
    const fee = parseFloat(this.get("fee"));
    const amount = 0 | parseFloat(this.get("amount"));
    const symbol = this.get("currencySymbol");
    const feeAmount = fee * amount;
    const number = (feeAmount + amount).toFixed(2);
    return {
      number: number,
      text: symbol + number
    }
  }),

  didInsertElement: function() {
    const me = this;
    this._super(...arguments);

    // Copy address
    let copyLink = document.querySelector('.payment-methods__cryptocurrency .payment-methods__copy');
    copyLink.onclick = function (event) {
      me.set('copiedToClipboard', true);
      console.log('click');
      const cryptoAdress = me.get('crypto-address.wallet');
      var x = document.createElement("INPUT");
      x.setAttribute("type", "text");
      x.setAttribute("value", cryptoAdress);
      document.body.appendChild(x);
      x.select();
      document.execCommand('copy');
      x.style.display="none";
      if (!Element.prototype.remove) {
        Element.prototype.remove = function remove() {
          if (this.parentNode) {
            this.parentNode.removeChild(this);
          }
        };
      }
      x.remove();
    }

    //TODO: get region from 
    var region = 'RU';
    var input = document.getElementById('payment-methods__phone');
    window.itiCardPaymentMethod = window.intlTelInput(input, {
        initialCountry: region
    });

    $('.payment-methods__card input[name=payment-methods__birthday]').mask('00/00/0000', {
      onComplete: function(){
        $('.payment-methods__card input[name=payment-methods__phone]').focus();
      }
    });

    $('.payment-methods__card input[name=payment-methods__card-number]').mask('0000  0000  0000  0000', {
      onComplete: function(){
        $('.payment-methods__card input[name=payment-methods__full-name]').focus();
      }
    });

    $('.payment-methods__card input[name=payment-methods__valid_thru]').mask('00/00', {
      onComplete: function(){
        $('.payment-methods__card input[name=payment-methods__cvc]').focus();
      }
    });

    $('.payment-methods__card input[name=payment-methods__cvc]').mask('000', {
      onComplete: function(){
        $('.payment-methods__card input[name=payment-methods__terms-of-use]').focus();
      }
    });

    const amount = me.$(".payment-methods .payment-methods__amount"); // 30 USD
    amount.on("focus", function() {
      me.set("paymentMethodsErrorMessage", "");
      me.$(".payment-methods .payment-methods__amount").removeClass("error");
    });

    const birthday = $(".payment-methods__card input[name=payment-methods__birthday]");
    birthday.on("focus", function() {
      me.set("paymentMethodsErrorMessage", "");
      me.$(".payment-methods__card input[name=payment-methods__birthday]").removeClass("error");
    });

    const phone = $(".payment-methods__card input[name=payment-methods__phone]");
    phone.on("focus", function() {
      me.set("paymentMethodsErrorMessage", "");
      me.$(".payment-methods__card input[name=payment-methods__phone]").removeClass("error");
    });

    const terms = $(".payment-methods__card input[name=payment-methods__terms-of-use]");
    terms.change(function() {
      me.set("paymentMethodsErrorMessage", "");
    });





// TODO: контроль вводимых данных из одного пакета
    // var custom_options = {
    //   byPassKeys: [8, 9, 37, 38, 39, 40],
    //   translation: {
    //     '0': {pattern: /\d/},
    //     '9': {pattern: /\d/, optional: true},
    //     '#': {pattern: /\d/, recursive: true},
    //     'A': {pattern: /[a-zA-Z0-9]/},
    //     'S': {pattern: /[a-zA-Z]/}
    //   };
    // };


    // $('#form-step2 input[name=cardExpMM]').mask('00', {
    //   onComplete: function(){
    //     $('#form-step2 input[name=cardExpYY]').focus();
    //   }
    // });
    // $('#form-step2 input[name=cardExpYY]').mask('00', {
    //   onComplete: function(){
    //     $('#form-step2 input[name=cardCVV]').focus();
    //   }
    // });
    // $('#form-step2 input[name=cardCVV]').mask('000', {
    //   onComplete: function(){
    //     $('#form-step2 input[name=cardName]').focus();
    //   }
    // });

    //const card = $(".payment-methods__card input[name=payment-methods__card-number]").val();


  },
  actions: {

    generateNewAddress() {
      const url = "https://indacoin.com/inner/GetEthWallet";
      fetch(url, {
        method: 'POST',
        headers: {
          "content-type": "application/json",
          "accept": "*/*",
          "x-requested-with": "XMLHttpRequest",
          "origin": "https://indacoin.com"
        }
      }).then(function(response) {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok\r\n' + response.status);
      }).then(function(response) {
        if (response.d) {
          alert(response.d);
          return response.d;
        } else {
          alert("Something weird has happened, please try one more time, or try to use another browser");
          return;
        }
      }).catch(function(error) {
        alert("Something weird has happened, please try one more time, or try to use another browser");
      });
    },

    setPaymentMethod(method) {
      if (this.get("method") === method)
        return;
      this.set("method", method);
    },
    setCurrency(currency) {
      if (this.get("currency") === currency)
        return;
      this.set("currency", currency);
    },
    registerPayment() {
      const me = this;
      const amountUSD = Number(me.get("totalAmount.number"));
      const amountIndacoinUSD = Number(this.get("amount"));
      let phone = window.itiCardPaymentMethod.getNumber();
      // TODO: вынести элементы и val(), эл дальше используются
      let card = $(".payment-methods__card input[name=payment-methods__card-number]").val();
      const expiryDate = $(".payment-methods__card input[name=payment-methods__valid_thru]").val();
      const cvc = $(".payment-methods__card input[name=payment-methods__cvc]").val();
      const cardholder_name = $(".payment-methods__card input[name=payment-methods__full-name]").val();
      const bday = $(".payment-methods__card input[name=payment-methods__birthday]").val();
      const amount = amountUSD; // TODO: double?
      const terms = $(".payment-methods__card input[name=payment-methods__terms-of-use]").is(":checked");
      let inputType = "";
      switch(me.get("currency")) {
        case 'USD':
          inputType = 16;
          break;
        case 'EUR':
          inputType = 50;
          break;
        case 'RUB':
          inputType = 54;
          break;
        case 'AUD':
          inputType = 55;
          break;
        case 'GBP':
          inputType = 56;
      }

      // Validation
      if (!amount) {
        this.set("paymentMethodsErrorMessage", "Please input correct amount");
        this.$(".payment-methods .payment-methods__amount").addClass("error");
        return;
      }

      if (!bday) {
        this.set("paymentMethodsErrorMessage", "Please enter correct birthday");
        this.$(".payment-methods__card input[name=payment-methods__birthday]").addClass("error");
        return;
      }

      // More terminated answers https://intl-tel-input.com/node_modules/intl-tel-input/examples/gen/is-valid-number.html
      if (!window.itiCardPaymentMethod.isValidNumber()) {
        this.set("paymentMethodsErrorMessage", "Please enter correct phone");
        this.$(".payment-methods__card input[name=payment-methods__phone]").addClass("error");
        return;
      }

      if (!terms) {
        this.set("paymentMethodsErrorMessage", "You must agree with Terms of Use");
        return;
      }

      // Change to sending
      card = card.replace(/ {2}/g, ' ');
      phone = phone.replace('+', '');

      const card_data = JSON.stringify({
        card: card, // 45653405 1918 1845
        expiryDate: expiryDate, // 11/19
        cvc: cvc // 123
      });

      const addInfo = JSON.stringify({
          amountUSD: amountUSD, //234
          amountIndacoinUSD: amountIndacoinUSD, //212.94
          phone: phone, // 79102131231
          card_data: card_data,
          cardholder_name: cardholder_name, // ddd sssss
          bday: bday // 11_11_2000
      });

      const data = JSON.stringify({
        addInfo: addInfo,
        amount: amount, // 234
        inputType: inputType // 16
      });

      const url = "https://indacoin.com/funding/RegisterPayment";
      fetch(url, {
        method: 'POST',
        headers: {
          "content-type": "application/json",
          "accept": "*/*",
          "x-requested-with": "XMLHttpRequest",
          "origin": "https://indacoin.com"
        },
        body: data
      }).then(function(response) {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok\r\n' + response.status);
      }).then(function(response) {

        if (response.d == "Card is not 3ds") {
          alert("Card is not 3ds");
          return;
        }
        if (response.d == "-3") {
          alert("Your account is blocked. To unblock please ask to support");
          return;
        }

        // custom check incoming bank payment link
        const re = /http/gi;
        const OK = re.exec(response.d);
        if (!OK) {
          alert("Something weird has happened, please try one more time, or try to use another browser");
          return false;
        }

        if (inputType == 15 || inputType == 13 || inputType == 18 || inputType == 17 || inputType == 16 || inputType == 21 || inputType == 22
                            || inputType == 23 || inputType == 24 || inputType == 25 || inputType == 30 || inputType == 50 || inputType == 36
                            || inputType == 54 || inputType == 55 || inputType == 56) {
          location.href = response.d;
          return;
        }
        if (inputType == 33 || inputType == 37 || inputType == 40 || inputType == 43 || inputType == 44 || inputType == 45 || inputType == 46
                            || inputType == 47 || inputType == 48) {
          alert(response.d);
          return;
        }
        else {
          const result = $.parseJSON(i.d);
          if (n == 3 || n == 7 || n == 8 || n == 29) {
            alert(result);
            return;
          } else {
            alert("Something weird has happened, please try one more time, or try to use another browser");
            return;
          }
        }
      }).catch(function(error) {
        alert("Something weird has happened, please try one more time, or try to use another browser");
      });

      // success: function(i) {
      //   if (loadingEffect("#depositButton", !1), t.disabled = !1, i.d == "Card is not 3ds") {
      //     alert("Card is not 3ds");
      //     return
      //   }
      //   if (i.d == "-3") {
      //     alert(window.t.blocked);
      //     return
      //   }
      //   if (n == 15 || n == 13 || n == 18 || n == 17 || n == 16 || n == 21 || n == 22 || n == 23 || n == 24 || n == 25 || n == 30 || n == 50 || n == 36 || n == 54) {
      //     location.href = i.d;
      //     return
      //   }
      //   if (n == 33 || n == 37 || n == 40 || n == 43 || n == 44 || n == 45 || n == 46 || n == 47 || n == 48) $("#hiddenDiv").html(i.d), document.forms.CashInCreditCards.submit();
      //   else {
      //     var r = $.parseJSON(i.d);
      //     n == 3 || n == 7 || n == 8 || n == 29 ? ($("#hiddenDiv").html(r.window), document.forms.CreditCards.submit()) : openMyModal2(r.window)
      //   }
      // },
      // error: function(n) {
      //   loadingEffect("#depositButton", !1);
      //   $.inArray(n.status, window.processedStatuses) == -1 && alert(window.t.er1);
      //   t.disabled = !1
      // }
    }
  },
  willDestroyElement() {
    const el = document.querySelector('.payment-methods__cryptocurrency .payment-methods__copy');
    el.removeAttribute('onclick');

    this._super(...arguments);
  }
});


// TODO: rewrite to helper
function precision(a) {
  if (!isFinite(a)) return 0;
  var e = 1, p = 0;
  while (Math.round(a * e) / e !== a) { e *= 10; p++; }
  return p;
}