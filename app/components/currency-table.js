import Component from '@ember/component';
import { computed } from '@ember/object';
import Table from 'ember-light-table';

export default Component.extend({
  model: [
    {
      avatar: '',
      coin: 'BCH',
      price: 0.00019081,
      change24h: '1.98%',
      change1w: '-4.98%',
      change1m: '2.65%',
    },
    {
      avatar: '',
      coin: 'BCH',
      price: 0.00019081,
      change24h: '1.98%',
      change1w: '-4.98%',
      change1m: '2.65%',
    },
    {
      avatar: '',
      coin: 'BCH',
      price: 0.00019081,
      change24h: '1.98%',
      change1w: '-4.98%',
      change1m: '2.65%',
    },
  ],

  columns: computed(function() {
    return [
      {
        label: '',
        valuePath: 'avatar',
        width: '20px',
      },
      {
        label: 'Coin',
        valuePath: 'coin',
      },
      {
        label: 'Price',
        valuePath: 'price',
        width: '80px',
      },
      {
        label: 'Change: 24h',
        valuePath: 'change24h',
      },
      {
        label: '1w',
        valuePath: 'change1w',
      },
      {
        label: '1m',
        valuePath: 'change1m',
      },
    ];
  }),

  table: computed('model', function() {
    return new Table(this.get('columns'), this.get('model'), { enableSync: true });
  }),
});
