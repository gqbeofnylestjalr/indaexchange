import Component from '@ember/component';
import ENV from 'indaexchange/config/environment';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
  ENV: ENV,
  store: service(),

  userSpinner: false,

  user: computed('model', function() {
    const me = this;
    console.log('UserLoading');
    //me.set("userSpinner", true);
    if (this.get('store').peekAll('pair').length)
      return this.get('store')
        .peekAll('pair')
        .objectAt(0)
        .get('userName');
    else return null;
  }),
});
