import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { task, timeout } from 'ember-concurrency';

//import ENV from 'indaexchange/config/environment';
//import helper from 'indaexchange/lib/helper';
//import { MY_CONSTANTS } from 'indaexchange/lib/helper';
//import ENVI from 'indaexchange/lib/helper';
//import acme from 'acme';

//import cookies from 'cookies'

export default Component.extend({
  //cookies: service(),
  store: service(),
  i18n: service(),

  method: 'exchange', // [ exchange-step-1, exchange-step-2, exchange-step-3, deposit, withdrawal ]
  exchangeStep: 1, // [ 1, 2, 3 ]
  paymentMethodsErrorMessage: '',
  paymentMethodsPhoneErrorMessage: '',

  /* Deposit start */
  depositCurrency: "BTC",
  qrCodeLink: computed("crypto-address", function () {
    const wallet = this.get("crypto-address").get("wallet");
    const hash = this.get("crypto-address").get("hash");
    return `https://indacoin.com/tools/ImageProxy.ashx?hash=${hash}&imgurl=http://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=${wallet}`;
  }),
  "crypto-address": computed('depositCurrency', function () {
    return this.store.peekRecord('crypto-address', this.get('depositCurrency'));
  }),

  copiedToClipboard: false,
  /* Deposit end */

  currency: 'USD', // [USD, EUR, RUB, AUD, GBP]
  currencySelected: { fullName: "Dollar", shortName: "USD" },
  cryptoCurrency: 'BTC', // [ BTC, ETH, LTC ... ]
  currencyList: computed('cash-info', function () {

    var currencies = {
      "USD": {
        fullName: "Dollar",
        shortName: "USD"
      },
      "EUR": {
        fullName: "Euro",
        shortName: "EUR"
      },
      "RUB": {
        fullName: "Rouble",
        shortName: "RUB"
      },
      "AUD": {
        fullName: "Australian Dollar",
        shortName: "AUD"
      },
      "GBP": {
        fullName: "Great Britain Pound",
        shortName: "GBP"
      }
    };

    let result = [];
    const records = this.get('store').peekAll('cash-info');
    records.forEach(function (item) {
      if (currencies[item.get('factCurrencyId')])
        result.push(
          currencies[item.get('factCurrencyId')]
        )
        //result.push(item.get('factCurrencyId'));
    });
    return result;
  }),

  filter: '',
  filteredCryptocurrency: computed('filter', 'cryptocurrency', function () {
    let filterTerm = this.get('filter');
    let currency = this.get('cryptocurrency');
    let showBitcoin = false;
    if (currency === 'BTC')
      showBitcoin = true;
    if (showBitcoin) {
      if (filterTerm.length == 0) {
        var filtered = this.get('store').peekAll('currency').filter(function (item) {
          if (!(item.get('short_name').toLowerCase() === 'btc' || item.get('short_name').toLowerCase() === 'eth')) {
            return true;
          }
        });
      }
      else {
        let noDoubledArray = [];
        var filtered = this.get('store').peekAll('currency').filter(function (item) {
          if (item.get('short_name').toLowerCase() !== 'btc') {
            if ((item.get('name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1) || (item.get('short_name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1)) {
              if (noDoubledArray.indexOf(item.get('short_name')) === -1) {
                noDoubledArray.push(item.get('short_name'));
                return true;
              }
            }
          }
        });
      }
    } else {
      if (filterTerm.length == 0) {
        var filtered = this.get('store').peekAll('currency');
      }
      else {
        let noDoubledArray = [];
        var filtered = this.get('store').peekAll('currency').filter(function (item) {
          if ((item.get('name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1) || (item.get('short_name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1)) {
            if (noDoubledArray.indexOf(item.get('short_name')) === -1) {
              noDoubledArray.push(item.get('short_name'));
              return true;
            }
          }
        });
      }
    }
    return filtered;
  }),

  currencySymbol: computed("currency", function () {
    const currency = this.get("currency");
    switch (currency) {
      case 'USD':
        return '$';
      case 'EUR':
        return '€';
      case 'RUB':
        return '₽';
      case 'AUD':
        return 'AU$';
      case 'GBP':
        return '£';
      default:
        return '$';
    }
  }),

  fee: computed("amount", "currencySymbol", function () {
    const currency = this.get("currency");
    const record = this.get('store').peekRecord('cash-info', currency);
    return record.get('feeFactPercent');
  }),

  feeMessage: computed("currencySymbol", function () {
    const currency = this.get("currency"); // 'USD'
    const symbol = this.get("currencySymbol");
    const record = this.get('store').peekRecord('cash-info', currency);

    const minSum = record.get('minSum') + symbol; // 30$
    const maxSum = record.get('maxSum') + symbol; // 3000$

    let feeFactPercent = record.get('feeFactPercent') * 100; // 9%
    if (precision(feeFactPercent) > 2)
      feeFactPercent = feeFactPercent.toFixed(2);

    let minFee = record.get('minFee'); // 2.7$
    if (precision(minFee) > 2)
      minFee = minFee.toFixed(2);

    const i18n = this.get('i18n');
    return Ember.String.htmlSafe(i18n.t('funds.deposit.fee-message', { minSum: minSum, maxSum: maxSum, feeFactPercent: feeFactPercent, minFee: minFee + symbol }));
  }),

  commision: computed("amount", "currencySymbol", function () {
    const fee = parseFloat(this.get("fee"));
    const amount = 0 | parseFloat(this.get("amount"));
    const symbol = this.get("currencySymbol");
    return symbol + (fee * amount).toFixed(2);
  }),

  totalAmount: computed("amount", "currencySymbol", function () {
    const fee = parseFloat(this.get("fee"));
    const amount = 0 | parseFloat(this.get("amount"));
    const symbol = this.get("currencySymbol");
    const feeAmount = fee * amount;
    const number = (feeAmount + amount).toFixed(2);
    return {
      number: number,
      text: symbol + number
    }
  }),

  currencyPrice: computed("currency", function () {
    const me = this;
    const currency = me.get("currency").toLowerCase(); // usd
    const cryptoCurrency = me.get("cryptoCurrency"); // BTC
    const price = me.store.peekRecord('currency', cryptoCurrency).get('price')[currency]; // 3968.50829
    return 1 / price; // 0.00025198385058684104
  }),

  amountInTask: task(function* () {
    const me = this;
    let amountIn = me.$(".exchange-calculator .exchange-calculator__from"); // 100 USD
    let amountOut = me.$(".exchange-calculator .exchange-calculator__to"); // 0.03 BTC

    const currencyInText = me.get('currency'); // USD
    const outCurrency = me.get('cryptoCurrency'); // BTC
    let amountInVal = amountIn.val();
    if (amountInVal === '') {
      amountOut.val('');
    } else {
      amountOut.val('...');
      amountInVal = amountInVal.replace(',', '.');
      const url = `https://indacoin.com/api/GetAltToAltMarketPrice/${currencyInText}/${outCurrency}/${amountInVal}`;
      const controller = new AbortController();
      const signal = controller.signal;
      try {
        fetch(url, {
          method: 'GET',
          signal: signal,
          headers: {
            "content-type": "application/json",
            "accept": "*/*",
            "x-requested-with": "XMLHttpRequest",
            "origin": "https://indacoin.com"
          }
        }).then(function (response) {
          if (response.ok) {
            return response.text();
          } else {
            amountOut.val('error');
          }
          throw new Error('Network response was not ok');
        }).then(function (data) {
          if (data < 0)
            data = 0.00000000;
          amountOut.val(parseFloat(data).toFixed(8));
        }).catch(function (error) {
          console.log(error);
        });
      } catch (e) {
        controller.abort();
      }
    }
  }).restartable(),

  amountOutTask: task(function* () {
    const me = this;
    let amountIn = me.$(".exchange-calculator .exchange-calculator__from"); // 100 USD
    let amountOut = me.$(".exchange-calculator .exchange-calculator__to"); // 0.03 BTC

    const currencyInText = me.get('currency'); // USD
    const outCurrency = me.get('cryptoCurrency'); // BTC
    let amountOutVal = amountOut.val();
    if (amountOutVal === '') {
      amountOut.val('');
    } else {
      amountIn.val('...');
      amountOutVal = amountOutVal.replace(',', '.');
      const url = `https://indacoin.com/api/GetAltToAltMarketPrice/${outCurrency}/${currencyInText}/${amountOutVal}`;
      const controller = new AbortController()
      const signal = controller.signal;
      try {
        fetch(url, {
          method: 'GET',
          signal: signal,
          headers: {
            "content-type": "application/json",
            "accept": "*/*",
            "x-requested-with": "XMLHttpRequest",
            "origin": "https://indacoin.com"
          }
        }).then(function (response) {
          if (response.ok) {
            return response.text();
          } else {
            amountOut.val('error');
          }
          throw new Error('Network response was not ok');
        }).then(function (data) {
          if (data < 0)
            data = 0.00000000;
          //amountOut.val(parseFloat(data).toFixed(8));
          amountIn.val(parseFloat(data).toFixed(8));
        }).catch(function (error) {
          console.log(error);
        });
      } catch (e) {
        controller.abort();
      }
    }
  }).restartable(),

  copyTask: task(function* () {
    const me = this;
    const cryptoAdress = me.get('crypto-address.wallet');
    var x = document.createElement("INPUT");
    x.setAttribute("type", "text");
    x.setAttribute("value", cryptoAdress);
    document.body.appendChild(x);
    x.select();
    document.execCommand('copy');
    x.style.display = "none";
    if (!Element.prototype.remove) {
      Element.prototype.remove = function remove() {
        if (this.parentNode) {
          this.parentNode.removeChild(this);
        }
      };
    }
    x.remove();
    me.set('copiedToClipboard', true);
    yield timeout(3000);
    me.set('copiedToClipboard', false);
  }).restartable(),

  didInsertElement: function () {
    const me = this;
    me._super(...arguments);


    //cookies.setCookie('cookie1', 'value1', { expires: new Date(new Date().getTime()+365*60*60*1000*24).toGMTString() });
    //console.log('Testing module: ' + cookies.getCookie('cookie1'));

    /****************************************/
    /********* Change inputs Limit **********/
    /****************************************/
    let amountIn = me.$(".exchange-calculator .exchange-calculator__from"); // 100 USD
    let amountOut = me.$(".exchange-calculator .exchange-calculator__to"); // 0.03 BTC

    // https://indacoin.com/api/GetAltToAltMarketPrice/USD/BTC/5000
    amountIn.on("keyup", function () {
      let task = me.get('amountInTask');
      task.perform();
    });

    amountIn.on("focus", function () {
      me.set("limitErrorMessage", "");
      me.$("#limit .operations__price").removeClass("error");
    });

    amountOut.on("keyup", function () {
      let task = me.get('amountOutTask');
      task.perform();
    });


    // limitAmount.on("keyup", function() {
    //   me.$("#limit .operations__amount").removeClass("error");
    //   if (limitPrice.val() === '' || limitAmount.val() === '') {
    //     limitTotal.val('');
    //   } else {
    //     let result = limitPrice.val() * limitAmount.val();
    //     limitTotal.val(result.toFixed(8));
    //   }
    // });
    // limitAmount.on("focus", function() {
    //   me.set("limitErrorMessage", "");
    // });

    // ****************************************
    // ******** Search dropdown ***************
    // ****************************************
    $('.exchange__cryptocurrency').each(function (index, dropdown) {

      //Find the input search box
      //let search = $(dropdown).find('.search');
      let search = me.get('filter');
      console.log(search);

      //Find every item inside the dropdown
      let items = $(dropdown).find('.dropdown-item');

      //Capture the event when user types into the search box
      $(search).on('input', function () {
        console.log('filter detect');
        filter($(search).val().trim().toLowerCase())
      });

      //For every word entered by the user, check if the symbol starts with that word
      //If it does show the symbol, else hide it
      function filter(word) {
        let length = items.length
        let collection = [];
        let hidden = 0
        for (let i = 0; i < length; i++) {
          if (items[i].value.toLowerCase().includes(word)) {
            $(items[i]).show()
          }
          else {
            $(items[i]).hide()
            hidden++
          }
        }

        //If all items are hidden, show the empty view
        if (hidden === length) {
          console.log('dropdown_empty show');
          $(dropdown).find('.dropdown_empty').show();
        }
        else {
          console.log('dropdown_empty hide');
          $(dropdown).find('.dropdown_empty').hide();
        }
      }

      //If the user clicks on any item, set the title of the button as the text of the item
      $(dropdown).find('.dropdown-menu').find('.menuItems').on('click', '.dropdown-item', function () {
        $(dropdown).find('.dropdown-toggle').text(
          $(this).children(".exchange__cryptocurrency__short-name").text()
        );
        $(dropdown).find('.dropdown-toggle').dropdown('toggle');
      })
    });

    // ****************************************
    // ****** End Search dropdown *************
    // ****************************************

    //TODO: get region from 
    var region = 'RU';
    var input = document.getElementById('exchange-form__phone');
    window.itiExchangeOperations = window.intlTelInput(input, {
      initialCountry: region
    });

    $('.exchange .exchange-form__card input[name=exchange-form__birthday]').mask('00/00/0000', { // 11_11_2000
      placeholder: "__/__/____",
      onComplete: function (birthday) {
        const me = this;
        const re = /^(0[1-9]|1[012])\/(0[1-9]|[12][0-9]|3[01])\/(19|20)\d\d$/gi;
        if (re.exec(birthday))
          me.$('.exchange-form__card input[name=exchange-form__phone]').focus();
        else
          me.$(".exchange-form__card input[name=exchange-form__birthday]").addClass("error");
      }
    });

    $('.exchange .exchange-form__card input[name=exchange-form__card-number]').mask('0000   0000   0000   0000', {
      onComplete: function () {
        $('.exchange-form__card input[name=exchange-form__full-name]').focus();
      }
    });

    $('.exchange .exchange-form__card input[name=exchange-form__valid_thru]').mask('00/00', {
      onComplete: function () {
        $('.exchange-form__card input[name=exchange-form__cvc]').focus();
      }
    });

    $('.exchange .exchange-form__card input[name=exchange-form__cvc]').mask('000', {
      onComplete: function () {
        $('.exchange-form__card input[name=exchange-form__terms-of-use]').focus();
      }
    });

    $('.exchange .exchange-form__card input[name=exchange-form__full-name]').bind('keyup', function () {
      $(this).val($(this).val().replace(/[^a-zA-Z-\s]/i, "").toUpperCase());
    });

    const birthday = $(".exchange .exchange-form__card input[name=exchange-form__birthday]");
    birthday.on("change", function () {
      console.log('change birthday')
      me.set("paymentMethodsPhoneErrorMessage", "");
      me.$(".exchange-form__card input[name=exchange-form__birthday]").removeClass("error");
    });

    const phone = $(".exchange .exchange-form__card input[name=exchange-form__phone]");
    phone.on("focus", function () {
      me.set("paymentMethodsPhoneErrorMessage", "");
      me.$(".exchange-form__card input[name=exchange-form__phone]").removeClass("error");
    });

    const terms = $(".exchange .exchange-form__card input[name=exchange-form__terms-of-use]");
    terms.on("focus", function () {
      me.set("paymentMethodsPhoneErrorMessage", "");
    });

    const card = $(".exchange .exchange-form__card input[name=exchange-form__card-number]");
    card.on("focus", function () {
      me.set("paymentMethodsErrorMessage", "");
      card.removeClass("error");
    });

    const expiryDate = $(".exchange .exchange-form__card input[name=exchange-form__valid_thru]");
    expiryDate.on("focus", function () {
      me.set("paymentMethodsErrorMessage", "");
      expiryDate.removeClass("error");
    });

    const cvc = $(".exchange .exchange-form__card input[name=exchange-form__cvc]");
    cvc.on("focus", function () {
      me.set("paymentMethodsErrorMessage", "");
      cvc.removeClass("error");
    });

    const cardholderName = $(".exchange .exchange-form__card input[name=exchange-form__full-name]");
    cardholderName.on("focus", function () {
      me.set("paymentMethodsErrorMessage", "");
      cardholderName.removeClass("error");
    });

    const amount = $(".exchange .exchange-form__amount");
    amount.on("focus", function () {
      console.log('amount.on("focus"');
      me.set("paymentMethodsErrorMessage", "");
      $(".exchange .exchange-form__amount").removeClass("error");
    });

  },

  actions: {
    copy() {
      const me = this;
      let task = me.get('copyTask');
      task.perform();
    },
    switchCryptoCurrency(value) {
      const me = this;
      me.set('cryptoCurrency', value);
      let task = me.get('amountOutTask');
      task.perform();
    },
    switchCurrency(value) {
      const me = this;
      me.set('currency', value.shortName);
      me.set('currencySelected',  value);
      let task = me.get('amountInTask');
      task.perform();
    },
    setPaymentMethod(method) {
      this.set("method", method);
    },
    nextStep() {
      const me = this;
      const step = me.get("exchangeStep");
      if (step === 1) {

        const amountIn = Number(me.$(".exchange-calculator .exchange-calculator__from").val()); // 100 USD
        const amountOut = Number(me.$(".exchange-calculator .exchange-calculator__to").val()); // 0.03 BTC

        if (!amountIn) {
          this.set("limitErrorMessage", "Please input correct amount");
          this.$("#limit .operations__amount").addClass("error");
          return;
        }

        if (!amountOut) {
          this.set("limitErrorMessage", "Please input correct amount");
          this.$("#limit .operations__amount").addClass("error");
          return;
        }

        me.incrementProperty("exchangeStep");

        // if (total > balance) {
        //   this.set("limitErrorMessage", "Your balance is not enough");
        //   this.$("#limit .operations__amount").addClass("error");
        //   return;
        // }

      } else if (step === 2) {

        const bday = $(".exchange-form__card input[name=exchange-form__birthday]").val();
        const terms = $(".exchange-form__card input[name=exchange-form__terms-of-use]").is(":checked");

        if (!bday) {
          me.set("paymentMethodsPhoneErrorMessage", "Please enter correct birthday");
          me.$(".exchange-form__card input[name=exchange-form__birthday]").addClass("error");
          return;
        }

        // TODO: translate with this codes https://github.com/jackocnr/intl-tel-input/blob/master/src/js/utils.js#L153
        const phoneStatus = window.itiExchangeOperations.getValidationError();
        switch (phoneStatus) {
          case intlTelInputUtils.validationError.INVALID_COUNTRY_CODE:
            me.set("paymentMethodsPhoneErrorMessage", "Invalid country code");
            me.$(".exchange-form__card input[name=exchange-form__phone]").addClass("error");
            return;
          case intlTelInputUtils.validationError.TOO_SHORT:
            me.set("paymentMethodsPhoneErrorMessage", "Phone is too short");
            me.$(".exchange-form__card input[name=exchange-form__phone]").addClass("error");
            return;
          case intlTelInputUtils.validationError.TOO_LONG:
            me.set("paymentMethodsPhoneErrorMessage", "Phone is too long");
            me.$(".exchange-form__card input[name=exchange-form__phone]").addClass("error");
            return;
          case intlTelInputUtils.validationError.NOT_A_NUMBER:
            me.set("paymentMethodsPhoneErrorMessage", "Not a number");
            me.$(".exchange-form__card input[name=exchange-form__phone]").addClass("error");
            return;
        }

        if (!terms) {
          me.set("paymentMethodsPhoneErrorMessage", "You must agree with Terms of Use");
          return;
        }

        me.incrementProperty("exchangeStep");
      }

    },

    generateNewAddress() {
      const url = "https://indacoin.com/inner/GetEthWallet";
      fetch(url, {
        method: 'POST',
        headers: {
          "content-type": "application/json",
          "accept": "*/*",
          "x-requested-with": "XMLHttpRequest",
          "origin": "https://indacoin.com"
        }
      }).then(function (response) {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok\r\n' + response.status);
      }).then(function (response) {
        if (response.d) {
          alert(response.d);
          return response.d;
        } else {
          alert("Something weird has happened, please try one more time, or try to use another browser");
          return;
        }
      }).catch(function (error) {
        alert("Something weird has happened, please try one more time, or try to use another browser");
      });
    },

    registerPayment() {
      const me = this;
      const amountUSD = Number(me.get("totalAmount.number"));
      const amountIndacoinUSD = Number(this.get("amount"));
      const phone = window.itiExchangeOperations.getNumber().replace(/\+/g, ''); // Delete all pluses
      // TODO: вынести элементы и val(), эл дальше используются
      const card = $(".exchange .exchange-form__card input[name=exchange-form__card-number]").val().replace(/ {2}/g, ' ');
      const expiryDate = $(".exchange .exchange-form__card input[name=exchange-form__valid_thru]").val();
      const cvc = $(".exchange .exchange-form__card input[name=exchange-form__cvc]").val();
      const cardholderName = $(".exchange .exchange-form__card input[name=exchange-form__full-name]").val();
      const bday = $(".exchange .exchange-form__card input[name=exchange-form__birthday]").val();
      const amount = amountUSD; // TODO: double?
      let inputType = "";
      switch (me.get("currency")) {
        case 'USD':
          inputType = 16;
          break;
        case 'EUR':
          inputType = 50;
          break;
        case 'RUB':
          inputType = 54;
          break;
        case 'AUD':
          inputType = 55;
          break;
        case 'GBP':
          inputType = 56;
      }

      // Validation
      if (!amount) {
        console.log('Set amount error class');
        me.set("paymentMethodsErrorMessage", "Please input correct amount");
        me.set('amountError', true);
        $(".exchange .exchange-form__amount").addClass("error");
        return;
      }

      if (!card) {
        this.set("paymentMethodsErrorMessage", "Please input correct card number");
        $(".exchange input[name=exchange-form__card-number]").addClass("error");
        return;
      }

      if (!cardholderName) {
        this.set("paymentMethodsErrorMessage", "Please input correct full name");
        $(".exchange input[name=exchange-form__full-name]").addClass("error");
        return;
      }

      if (!expiryDate) {
        this.set("paymentMethodsErrorMessage", "Please input correct expiry date");
        $(".exchange input[name=exchange-form__valid_thru]").addClass("error");
        return;
      }

      if (!cvc) {
        this.set("paymentMethodsErrorMessage", "Please input correct CVC");
        $(".exchange input[name=exchange-form__cvc]").addClass("error");
        return;
      }

      const card_data = JSON.stringify({
        card: card, // 45653405 1918 1845
        expiryDate: expiryDate, // 11/19
        cvc: cvc // 123
      });

      const addInfo = JSON.stringify({
        amountUSD: amountUSD, //234
        amountIndacoinUSD: amountIndacoinUSD, //212.94
        phone: phone, // 79102131231
        card_data: card_data,
        cardholder_name: cardholderName, // ddd sssss
        bday: bday // 11_11_2000
      });

      const data = JSON.stringify({
        addInfo: addInfo,
        amount: amount, // 234
        inputType: inputType // 16
      });

      console.log('Data');
      console.log(data);

      const url = "https://indacoin.com/funding/RegisterPayment";
      fetch(url, {
        method: 'POST',
        headers: {
          "content-type": "application/json",
          "accept": "*/*",
          "x-requested-with": "XMLHttpRequest",
          "origin": "https://indacoin.com"
        },
        body: data
      }).then(function (response) {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok\r\n' + response.status);
      }).then(function (response) {

        if (response.d == "Card is not 3ds") {
          alert("Card is not 3ds");
          return;
        }
        if (response.d == "-3") {
          alert("Your account is blocked. To unblock please ask to support");
          return;
        }

        // custom check incoming bank payment link
        const re = /http/gi;
        const OK = re.exec(response.d);
        if (!OK) {
          alert("Something weird has happened, please try one more time, or try to use another browser");
          return false;
        }

        if (inputType == 15 || inputType == 13 || inputType == 18 || inputType == 17 || inputType == 16 || inputType == 21 || inputType == 22
          || inputType == 23 || inputType == 24 || inputType == 25 || inputType == 30 || inputType == 50 || inputType == 36
          || inputType == 54 || inputType == 55 || inputType == 56) {
          location.href = response.d;
          return;
        }
        if (inputType == 33 || inputType == 37 || inputType == 40 || inputType == 43 || inputType == 44 || inputType == 45 || inputType == 46
          || inputType == 47 || inputType == 48) {
          alert(response.d);
          return;
        }
        else {
          const result = $.parseJSON(i.d);
          if (n == 3 || n == 7 || n == 8 || n == 29) {
            alert(result);
            return;
          } else {
            alert("Something weird has happened, please try one more time, or try to use another browser");
            return;
          }
        }
      }).catch(function (error) {
        alert("Something weird has happened, please try one more time, or try to use another browser");
      });
    },

    prevStep() {
      this.decrementProperty("exchangeStep");
    },
    setCurrency(currency) {
      if (this.get("currency") === currency)
        return;
      this.set("currency", currency);
    }
  },
  willDestroyElement() {
    const el = document.querySelector('.exchange .exchange-form__cryptocurrency .exchange-form__copy');
    el.removeAttribute('onclick');
    this._super(...arguments);
  }
});

// TODO: rewrite to helper
function precision(a) {
  if (!isFinite(a)) return 0;
  var e = 1, p = 0;
  while (Math.round(a * e) / e !== a) { e *= 10; p++; }
  return p;
}