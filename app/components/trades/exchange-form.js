import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  store: service(),
  item: null,

  cryptocurrency: 'BTC',
  cryptocurrencyList: computed('balance', function() {
    let result = [];
    this.get('balance').forEach(function(item) {
      result.push(item.get('currency'));
    });
    return result;
  }),

  filter: '',
  //dropdownCurrency: 'BTC',

  filteredCryptocurrency: computed('filter', 'cryptocurrency', function() {
    let filterTerm = this.get('filter');
    let currency = this.get('cryptocurrency');
    let showBitcoin = false;
    if (currency === 'BTC')
      showBitcoin = true;

    if (showBitcoin) {
      if (filterTerm.length == 0) {
        var filtered = this.get('store').peekAll('currency').filter( function(item) {
          if (!(item.get('short_name').toLowerCase() === 'btc' || item.get('short_name').toLowerCase() === 'eth')) {
            return true;
          }
        });
      }
      else {
        let noDoubledArray = [];
        var filtered = this.get('store').peekAll('currency').filter( function(item) {

          if (item.get('short_name').toLowerCase() !== 'btc') {
            if ((item.get('name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1) || (item.get('short_name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1)) {
              if (noDoubledArray.indexOf(item.get('short_name')) === -1) {
                noDoubledArray.push(item.get('short_name'));
                return true;
              }
            }
          }

        });
      }
    } else {
      if (filterTerm.length == 0) {
        var filtered= this.get('store').peekAll('currency');
      }
      else {
        let noDoubledArray = [];
        var filtered = this.get('store').peekAll('currency').filter( function(item) {
          if ((item.get('name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1) || (item.get('short_name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1)) {
            if (noDoubledArray.indexOf(item.get('short_name')) === -1) {
              noDoubledArray.push(item.get('short_name'));
              return true;
            }
          }
        });
      }
    }
    return filtered;
  }),
  
  didInsertElement: function() {
    const me = this;
    this._super(...arguments);

// ****************************************
// ******** Search dropdown ***************
// ****************************************
    $('.dropdown').each(function(index, dropdown){

      //Find the input search box
      let search = $(dropdown).find('.search');
      
      //Find every item inside the dropdown
      let items = $(dropdown).find('.dropdown-item');
      
      //Capture the event when user types into the search box
      $(search).on('input', function(){
          filter($(search).val().trim().toLowerCase())
      });
      
      //For every word entered by the user, check if the symbol starts with that word
      //If it does show the symbol, else hide it
      function filter(word) {
          let length = items.length
          let collection = []
          let hidden = 0
          for (let i = 0; i < length; i++) {
              if (items[i].value.toLowerCase().includes(word)) {
                  $(items[i]).show()
              }
              else {
                  $(items[i]).hide()
                  hidden++
              }
          }
      
          console.log('hidden = ' + hidden + ' length = ' + length);
          //If all items are hidden, show the empty view
          if (hidden === length) {
            console.log('dropdown_empty show');
            $(dropdown).find('.dropdown_empty').show();
          }
          else {
            console.log('dropdown_empty hide');
            $(dropdown).find('.dropdown_empty').hide();
          }
      }
      
      //If the user clicks on any item, set the title of the button as the text of the item
      $(dropdown).find('.dropdown-menu').find('.menuItems').on('click', '.dropdown-item', function(){
          $(dropdown).find('.dropdown-toggle').text($(this)[0].value);
          $(dropdown).find('.dropdown-toggle').dropdown('toggle');
      })
    });

// ****************************************
// ****** End Search dropdown *************
// ****************************************




    //$('.selectpicker').selectpicker(); // TODO: conctrete targets

    // https://indacoin.com/api/GetAltToAltMarketPrice/DARK/ACT/33

    let amountIn = me.$("#trades__amount-in");
    const amountOut = me.$("#trades__amount-out");

    amountIn.on("keyup", function() {
      const currencyInText = me.get('item').short_name;
      const outCurrency = me.get('cryptocurrency');
      let amountInVal = amountIn.val();
      if (amountInVal === '') {
        amountOut.val('');
      } else {
        amountInVal = amountInVal.replace(',', '.');
        const url = `https://indacoin.com/api/GetAltToAltMarketPrice/${outCurrency}/${currencyInText}/${amountInVal}`;
        fetch(url, {
          method: 'GET',
          headers: {
            "content-type": "application/json",
            "accept": "*/*",
            "x-requested-with": "XMLHttpRequest",
            "origin": "https://indacoin.com"
          }
        }).then(function(response) {
          if(response.ok) {
            return response.text();
          }
          throw new Error('Network response was not ok');
        }).then(function(data) {
          if (data < 0)
            data = 0.00000000;
            amountOut.val(parseFloat(data).toFixed(8));
        }).catch(function(error) {
          alert(error);
        });
      }
    });

  },
  actions: {
    switchCurrency(value) {
      this.set('cryptocurrency', value);
    },
    // https://indacoin.com/inner/CreateOrderEx
    // {"currencyInText":"DARK","outCurrency":"ACT","amountIn":23,"amountWantedToBuy":23}
    createOrderEx() {
      const me = this;
      const currencyInText = me.get('item').short_name;
      const outCurrency = me.get('cryptocurrency').currency;
      const amountIn =  $('#trades__amount-in').val();
      const amountWantedToBuy = $('#trades__amount-out').val();
      const data = JSON.stringify({
        currencyInText: currencyInText,
        outCurrency: outCurrency,
        amountIn: amountIn,
        amountWantedToBuy: amountWantedToBuy
      });
      const url = "https://indacoin.com/inner/CreateOrderEx";
      fetch(url, {
        method: 'POST',
        headers: {
          "content-type": "application/json",
          "accept": "*/*",
          "x-requested-with": "XMLHttpRequest",
          "origin": "https://indacoin.com"
        },
        body: data
      }).then(function(response) {
        if(response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok');
      }).then(function(response) {
        if (response.d) {
          alert(response.d);
        }
        else
          throw new Error(`Failure`);
      }).catch(function(error) {
        alert(error);
      });
    }
  }
});
