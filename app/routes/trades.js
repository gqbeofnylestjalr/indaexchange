import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  model: function() {
    const me = this;
    if (typeof FastBoot === 'undefined') {
      return RSVP.hash({
        currency: this.get('store')
          .findAll('currency')
          .then(categories => {
            return categories.filter(category => {
              return category.get('isActive') === true;
            });
          }),
        pair: me.modelFor('application').pair,
      });
    }
  },

  setupController: function(controller, model) {
    const me = this;
    me._super(controller, model);
    if (typeof FastBoot === 'undefined') {
      controller.set('balance', me.store.peekAll('balance'));
    }
  },
});
