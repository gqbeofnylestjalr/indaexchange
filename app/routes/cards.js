import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  model: function() {
    const me = this;
    if (typeof FastBoot === 'undefined') {
      return RSVP.hash({
        cards: me.get('store').query('card'),
      });
    }
  },

  setupController: function(controller, model) {
    const me = this;
    me._super(controller, model);
    controller.set('phoneVerification', me.store.peekRecord('phone', 1));
  },
});
