import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({

  model: function(){
    const me = this;
    if (typeof FastBoot === 'undefined') {
      return RSVP.hash({
        pair: me.modelFor('application').pair,
        currency: me.get('store').findAll('currency')
      });
    }
  },

  setupController: function(controller, model) {
    const me = this;
    me._super(controller, model);
    if (typeof FastBoot === 'undefined') {
      controller.set('balance', me.store.peekAll('balance'));
    }
  }

});
