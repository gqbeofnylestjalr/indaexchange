import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({

  model: function(){
    const me = this;
    if (typeof FastBoot === 'undefined') {
      return RSVP.hash({
        pair: me.modelFor('application').pair,
        currency: me.get('store').findAll('currency').then((categories)=>{
          return categories.filter((category)=>{
              return category.get('withdrawEnabled') === true;
          });
        }),
        "history-user": me.get('store').query('history-user', { operationFilter: 2 }) // only withdrawal
      });
    }
  },

  // setupController: function(controller, model) {
  //   const me = this;
  //   me._super(controller, model);
  //   controller.set('balance', me.store.peekAll('balance'));
  // }
  setupController: function(controller, model) {
    const me = this;
    me._super(controller, model);

    // let res = me.store.peekAll('balance').filter( function(bal) {
    //   me.store.peekAll('currency').forEach(function(cur){
    //     if (cur.get('short_name') === bal.get('currency')) return true;
    //   })
    // })
    // controller.set('balance', res);
    //me.store.peekAll('balance').forEach(function(item){console.log(item.get('currency'))})

    if (typeof FastBoot === 'undefined') {
      controller.set('balance', me.store.peekAll('balance'));
    }
  }


});
