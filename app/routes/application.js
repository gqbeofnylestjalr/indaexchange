import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import { task, timeout } from 'ember-concurrency';

export default Route.extend({
  pollServerForChanges: task(function*() {
    while (true) {
      yield timeout(1000 * 60 * 5);
      this.get('store').query('pair', { pairName: 'ETH_BTC' });
    }
  })
    .on('activate')
    .cancelOn('deactivate')
    .restartable(),

  model: function() {
    const me = this;
    if (typeof FastBoot === 'undefined') {
      return RSVP.hash({
        pair: me.get('store').query('pair', { pairName: 'ETH_BTC' }),
      });
    }
  },
});
