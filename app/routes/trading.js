import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  // TODO: в промисе после загрузки ticker брать первую запись и загружать оффер, прокидывая в контроллер свойство pair = BTC_USD
  // TODO: замокать loader на элементы загрузки табличек, на кнопки. Снимать loader после выполнения промиса
  model: function() {
    const me = this;
    const store = me.get('store');
    if (typeof FastBoot === 'undefined') {
      return me
        .get('store')
        .findAll('ticker')
        .then(function(ticker) {
          const pairName = ticker.get('firstObject').get('id');
          return RSVP.hash({
            ticker: ticker,
            'history-user': store.query('history-user', { operationFilter: 143 }),
            offer: store.query('offer', { pairName: pairName }),
            'history-trade': store.query('history-trade', {
              pairName: pairName,
              count: 30,
            }),
            pair: me.modelFor('application').pair,
            currency: store.findAll('currency'),
          });
        });
    }
  },

  setupController: function(controller, model) {
    const me = this;
    me._super(controller, model);

    if (typeof FastBoot === 'undefined') {
      controller.set('currentOrdersData', me.store.peekAll('current-order'));
      controller.set('currencyChartData', me.store.peekAll('graph'));

      controller.set('pair', model.ticker.get('firstObject').get('id'));
      controller.set(
        'selectedQuote',
        model.ticker
          .get('firstObject')
          .get('id')
          .split('_')[1],
      );
    }

    // controller.set("graphSpinner", false);
    // console.log("graphSpinner off");
  },
  // ,actions: {
  //   loading(transition, originRoute) {
  //     let controller = this.controllerFor('exchange');
  //     controller.set('graphSpinner', true);
  //     transition.promise.finally(function() {
  //         controller.set('graphSpinner', false);
  //         controller.set("orderbookSpinner", false);
  //     });
  //   }
  // }
});
