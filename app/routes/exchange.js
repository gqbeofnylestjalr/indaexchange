import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  model: function(params) {
    const me = this;
    let hash = {};
    let uid = params.user;
    if (uid) {
        hash.user = uid;
    }
    if (typeof FastBoot === 'undefined') {
      return RSVP.hash({
        pair: me.modelFor('application').pair,
        currency: me.get('store').findAll('currency'),
        'history-user': me.get('store').query('history-user', { operationFilter: 1 }),
        'cash-info': me.get('store').query('cash-info', hash),
      });
    }
  },

  setupController: function(controller, model) {
    const me = this;
    me._super(controller, model);
    if (typeof FastBoot === 'undefined') {
      controller.set('balance', me.store.peekAll('balance'));
    }
  },
});
