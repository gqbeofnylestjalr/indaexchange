import Locale from 'ember-i18n/utils/locale';
import { makeArray } from '@ember/array';
import { getOwner } from '@ember/application';
import { get } from '@ember/object';

const FALLBACK_LOCALE = 'en';

let missingMessage = function(locale, key, data) {

    let i18n = this;

    let count = get(data, 'count');

    let defaults = makeArray(get(data, 'default'));
    defaults.unshift(key);

    let localeObj = new Locale(FALLBACK_LOCALE, getOwner(i18n));
    let template = localeObj.getCompiledTemplate(defaults, count);
    return template(data);
};

export default missingMessage;